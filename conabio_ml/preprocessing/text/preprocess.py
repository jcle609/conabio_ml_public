#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

import errno
import json
import threading
import os
import pandas
import pydash
import re
import nltk
import spacy
import string

from gensim.corpora import Dictionary
from multiprocessing import Pool, Manager, Value
from spacy.tokens.doc import Doc

from pdb import set_trace as bp

translator = str.maketrans('', '', string.punctuation)
nlp = spacy.load('en', disable=['parser', 'ner'])
re_digits = re.compile(r'\d+[\.\d]*([e|E][\d]*)*\b')

lock = threading.Lock()


class PreProcess():

    descriptor = {"processed": False,
                  "stop_words": False,
                  "lemmatization": False,
                  "format": "csv",
                  "destination": None}

    def __init__(self, path, data, descriptor, id_dataset, processes=4):
        self.dataset_descriptor = descriptor
        self.id_dataset = id_dataset
        self.processes = processes
        self.data = data
        self.path = path

    def __apply(self, data, to_apply):
        pool = Pool(processes=self.processes)
        results = pool.map(to_apply, data)
        pool.close()
        pool.join()

        return results

    def __create_path(self, filename):
        with lock:
            if not os.path.exists(os.path.dirname(filename)):
                try:
                    os.makedirs(os.path.dirname(filename))
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise

    def save(self, processed_data, filename=None, format="csv"):
        # Check compatibility
        try:
            if format == "csv":
                data_shape = self.data["data"].shape
                try:
                    if len(processed_data) == data_shape[0]:
                        # New columns stored and data save
                        self.data["processed_data"] = processed_data
                        self.dataset_descriptor["dataset"]["fields"].append(
                            "processed_data")
                        to_store = self.data[self.dataset_descriptor["dataset"]["fields"]]
                        self.dataset_descriptor["preprocess"] = self.descriptor

                        if filename != None:
                            target_file = os.path.join(self.path,
                                                       "preprocessing",
                                                       filename+"."
                                                       + self.descriptor["format"])
                            self.__create_path(target_file)
                            to_store.to_csv(target_file,
                                            sep=',', encoding='utf-8')
                            self.dataset_descriptor["preprocess"]["destination"] = filename

                            with open(os.path.join(self.path, self.id_dataset + '.json'), 'w') as outfile:
                                json.dump(self.dataset_descriptor, outfile)

                        return to_store, self.dataset_descriptor
                    else:
                        raise Exception(
                            "Processed data has to be same length than dataset")
                except:
                    # TODO: Check type of error
                    raise
            else:
                raise Exception("Store in format ", format, " not implemented")
        except:
            raise

    def load(self):
        path = os.path.join(self.path,
                            "preprocessing",
                            self.dataset_descriptor["preprocess"]["destination"] +
                            "."+self.dataset_descriptor["preprocess"]["format"]
                            )

        self.data = pandas.read_csv(path, index_col=0)
        return self.data, self.dataset_descriptor

    def build_dict(self, data):
        # Here we have to check if preprocessed field exists
        # An option is to send data to build dict
        texts = [d.split() for d in data]

        dct = Dictionary(texts)
        return dct

    def process(self, data):
        res = self.__apply(data, process_line)

        ret = pydash.chain(res)
        ret = ret.map(lambda x: " ".join(x))

        self.descriptor["processed"] = True

        return ret.flatten().value()

    def remove_stop_words(self, data):
        res = self.__apply(data, stop_words_line)

        ret = pydash.chain(res)
        ret = ret.map(lambda x: " ".join(x)).flatten()

        self.descriptor["stop_words"] = True

        return ret.value()

    def lemmatization(self, data):
        res = self.__apply(data, lemmatization_line)

        ret = pydash.chain(res)
        ret = ret.map(lambda x: " ".join(x)).flatten()

        self.descriptor["lemmatization"] = True

        return ret.value()


def process_line(text_chunk):
    """
    Common processing for text chunk is made
    Digit remotion
    Special & accented values replacing
    Indentation for words

    Parameters
    ----------
    text_chunk : str
        text to be preoceseed
    """

    if not text_chunk:
        return ""

    text_chunk = re.sub(re_digits, " _DIGITO_ ", text_chunk)
    tokens = nltk.word_tokenize(text_chunk)
    words = [word.lower() for word in tokens]

    return words


def stop_words_line(txt):
    if type(txt) != Doc:
        txt = nlp(txt)

    words = [str(token) for token in txt if not token.is_stop]

    return words


def lemmatization_line(txt, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
    """https://spacy.io/api/annotation"""

    if type(txt) != Doc:
        txt = nlp(txt)

    res = [token.lemma_ for token in txt if token.pos_ in allowed_postags]
    return res
