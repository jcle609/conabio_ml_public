#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import linecache


def build_and_conds(arr, lbl='', parenthesis=False):
    return build_conds(arr, lbl, 'AND', parenthesis)


def build_or_conds(arr, lbl='', parenthesis=False):
    return build_conds(arr, lbl, 'OR', parenthesis)


def build_conds(arr, lbl, type_cond, parenthesis=False):
    if not arr:
        return ''
    ret_str = ''
    for elem in arr:
        if not elem:
            continue
        if not ret_str == '':
            ret_str += ' %s ' % type_cond
        ret_str += '%s:%s' % (lbl, elem) if lbl else elem
    if parenthesis and type_cond in ret_str:
        ret_str = '(%s)' % ret_str
    return ret_str
