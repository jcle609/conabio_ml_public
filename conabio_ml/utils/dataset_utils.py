#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import time
from multiprocessing import Pool
from urllib.request import urlretrieve
from .logger import get_logger

logger = get_logger(__name__)


def get_all_files(path, seek_extension="txt"):
    """
    Retrieve all files in a folder on format
        path, relative path (from path), file name

    Parameters
    ----------
    path : str
        Path to find all files

    seek_extension : str
        Extension of file needed

    Returns
    -------
    List
        List of found files
    """
    paths = []

    for f in [name for name in os.listdir(path)]:
        if f.startswith("."):
            continue

        if os.path.isdir(os.path.join(path, f)):
            temps = get_all_files(os.path.join(path, f),
                                  seek_extension=seek_extension)
            paths += temps
        elif f.lower().endswith(seek_extension):
            if os.path.isfile(os.path.join(path, f)):
                paths += [{"path": path,
                           "title": f}]

    return paths


def parallel_download(urls_filenames):
    pool = Pool()
    pool.map(download_file_job, urls_filenames)
    pool.close()
    pool.join()


def download_file_job(p):
    try:
        return download_file(*p)
    except Exception as e:
        logger.exception("Exception in download")


def download_file(url, fname):
    tic = time.time()

    def _progress(count, block_size, total_size):
        perc = int(min(float(count * block_size) / total_size * 100.0, 100))
        sys.stdout.write('\r>> Downloading %s %.1f%%' % (fname, perc))
        sys.stdout.flush()
    if not os.path.exists(fname):
        urlretrieve(url, fname, _progress)


def is_array_like(obj):
    return hasattr(obj, '__iter__') and hasattr(obj, '__len__') and \
        not type(obj) == str
