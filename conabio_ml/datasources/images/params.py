snmb_url = 'https://snmb.conabio.gob.mx'
snmb_contributor = 'Sistema Nacional de Monitoreo de la Biodiversidad'
snmb_description = 'SNMB 2019 Dataset'
join_anot = '&fq={{!join from=ftrampa_id to=id fromIndex=anotaciones }} {0}'
join_coll = '&fq={{!join from=id to=ftrampa_id fromIndex={0} }} {1}'
fct_fld_rq = '&facet=true&facet.field={0}&facet.mincount=1&'\
             'facet.limit=-1&facet.sort=count'
host_solr = 'http://snmb.conabio.gob.mx'
solr_qry = host_solr+'/solr/{0}/select?rows={1}&start={2}&wt=json&q={3}'
rowspp = 100
host_imgs = {
    'snmb':'http://coati.conabio.gob.mx/archivos/',
    'ibunam':'http://snmb.conabio.gob.mx/ibunam_files/',
    'pronatura_ver':'http://snmb.conabio.gob.mx/pronatver_files/',
    'serengeti':'https://snapshotserengeti.s3.msi.umn.edu/',
    'ibiologia':'http://snmb.conabio.gob.mx/ibunam_files/'
}
solr_dateformat = '%Y-%m-%dT%H:%M:%SZ'
std_dateformat = '%Y-%m-%d %H:%M:%S'
taxa_lvls = [
    "especie", "genero", "familia", "orden", "clase", "phylum", "reino"]
taxa_indexes = {
    "especie": [50,19], "genero": [44,13], "familia": [39,9], "orden": [35,7],
    "clase": [31,4], "phylum": [27,2], "reino": [1]
}
taxa_subspcs = ['subespecie', 'variedad', 'forma', 'raza']
images_filename_format = '{0}_{1}_{2}.json'
images_bboxes_filename_format = '{0}_{1}_{2}_bboxes.json'