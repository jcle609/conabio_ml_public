import json
import ntpath
import os
import sys
from datetime import date, datetime
import requests
from . import params
from ...utils.datasources_utils import build_or_conds, build_and_conds
from ...utils.logger import get_logger

logger = get_logger(__name__)


class DataSourceBuilder():
    """Builds a data source representation in JSON files. """

    def __init__(self,
                 collection,
                 year,
                 destination_path,
                 description,
                 version,
                 url=params.snmb_url,
                 contributor=params.snmb_contributor):
        """
        Parameters
        ----------
        collection : str
          Collection of the Datasource
        year : int
          Year of the Datasource version
        destination_path : str
          Folder where files will be stored
        description : str
          A description of the Datasource
        version : str
          Version of the Datasource
        url : str
          URL of the Datasource
        contributor : str
          Contributors of the Datasource
        """
        self.collection = collection
        self.year = int(year)
        self.destination_path = destination_path
        self.description = description
        self.version = version
        self.url = url
        self.contributor = contributor
        self.images_filename = \
            params.images_filename_format.format(collection, year, version)
        self.images_bboxes_filename = \
            params.images_bboxes_filename_format.format(
                collection, year, version)

    def build(self):
        """Build files with information from the data source.
        This method creates the following files:
          - `collection`_names.tsv
            TSV (tab-separated values) file with a dictionary of the category
            ids and category names with the following format:
              `category_id` `category_name`
            E.g.::
              34122 Canis latrans
              34123 Glossophaga leachii
              34126 Neotoma lepida
              34127 Noctilio leporinus
          - `collection`_is_a.txt
            Text file that stores the relationship of each category with its
            superior category (parent), allowing to generate the entire
            taxonomic path of any organism. The format of this file is as
            follows:
              `parent_id` `category_id`
            E.g.::
              129883 131072
              129884 131075
              129884 131076
          - `collection`_`year`_`version`.json
            JSON file que contains information of image-level annotations in
            the data source.
          - `collection`_`year`_`version`_bbox.json
            JSON file que contains information of bounding box annotations in
            the data source.
            Format in the image-level and bounding box annotations is::
              {
                "info": info,
                "items": [item],
                "annotations": [annotation],
                "categories": [category]
              }

            Where::
              info {
                "year": int,
                "version": str,
                "description": str,
                "contributor": str,
                "url": str,
                "date_created": datetime,
              }

              item {
                "id": str,
                "file_name": str,
                "url": str,
                "width": int,
                "height": int,
                "location": str,
                "date_captured": datetime
              }

              annotation {
                "id": int,
                "item_id": int,
                "category_id": int,
                "bbox": [x,y,width,height]  # only for bounding box annotations
              }

              category {
                "id": int,
                "name": str,
                "category_path": str
              }
        """
        (names, is_a) = self.get_names_and_is_a()
        names_path = os.path.join(
            self.destination_path, f"{self.collection}_names.tsv")
        names_str = ''
        for id, name in names.items():
            names_str += f"{id}\t{name}\n"
        with open(names_path, 'w') as out:
            logger.info(
                f"Writing 'names' file in {os.path.abspath(names_path)}")
            out.write(names_str)
        is_a_path = os.path.join(
            self.destination_path, f'{self.collection}_is_a.txt')
        is_a_str = ''
        for id, id_asc in is_a.items():
            is_a_str += f"{id_asc} {id}\n"
        with open(is_a_path, 'w') as out:
            logger.info(
                f"Writing 'is_a' file in {os.path.abspath(is_a_path)}")
            out.write(is_a_str)

        (imgs_qry, anns_qry) = self.get_queries(bboxes=False)
        categories = self.get_categories(imgs_qry, anns_qry)
        images = self.get_images(imgs_qry, anns_qry)
        annotations = self.get_annotations(imgs_qry, anns_qry, bboxes=False)
        data = {
            "info": {
                'year': self.year,
                'url': self.url,
                'contributor': self.contributor,
                'description': self.description,
                'date_created': date.today().strftime("%Y/%m/%d"),
                'version': self.version
            },
            "items": images,
            "annotations": annotations,
            "categories": categories
        }
        json_path = os.path.join(self.destination_path, self.images_filename)
        with open(json_path, 'w',) as outfile:
            logger.info(f"Writing json file without bboxes in {json_path}")
            json.dump(data, outfile)

        (imgs_qry, anns_qry) = self.get_queries(bboxes=True)
        categories = self.get_categories(imgs_qry, anns_qry)
        images = self.get_images(imgs_qry, anns_qry)
        annotations = self.get_annotations(imgs_qry, anns_qry, bboxes=True)
        data_bboxes = {
            "info": {
                'year': self.year,
                'url': self.url,
                'contributor': self.contributor,
                'description': self.description,
                'date_created': date.today().strftime("%Y/%m/%d"),
                'version': self.version
            },
            "items": images,
            "annotations": annotations,
            "categories": categories
        }
        json_path = os.path.join(
            self.destination_path, self.images_bboxes_filename)
        with open(json_path, 'w',) as outfile:
            logger.info(f"Writing json file with bboxes in {json_path}")
            json.dump(data_bboxes, outfile)

    def get_queries(self, bboxes=False):
        """Creates string queries to get infoarmation from the data source.

        Parameters
        ----------
        bboxes : bool, optional
          Whether to create queries for bounding box annotations or not
          (default is False)

        Returns
        -------
        (str, str)
          images query and annotations query
        """
        if bboxes:
            level_cond = build_or_conds([
                'nivel_anotador:usuario',
                'nivel_anotador:curador'
            ], parenthesis=True)
            type_cond = build_and_conds([
                'tipo_anotacion:especimen',
                'reino:Animalia'
            ])
        else:
            level_cond = '-nivel_anotador:algoritmo'
            type_cond = build_or_conds([
                build_and_conds([
                    'tipo_anotacion:marcas',
                    'marcas:*sin_fauna*'
                ], parenthesis=True),
                build_and_conds([
                    'tipo_anotacion:especimen',
                    'reino:Animalia'
                ], parenthesis=True)], parenthesis=True)
        imgs_qry = '-empty:*'
        anns_qry = build_and_conds([
            level_cond,
            type_cond,
            f'coleccion:{self.collection}'
        ])
        return imgs_qry, anns_qry

    def get_images(self, imgs_qry, anns_qry):
        """Consult information from a data source from a set of images and
        annotations queries and build the information related to each image.

        Parameters
        ----------
        imgs_qry : str
          Solr images query
        anns_qry : str
          Solr annotations query

        Returns
        -------
        List of dict
          List that contains the information of each image
        """
        total_rows = 1
        start = 0
        images = []
        while start < total_rows:
            base_qry = \
                params.solr_qry.format(
                    self.collection, params.rowspp, start, imgs_qry)
            join_qry = params.join_anot.format(anns_qry)
            qry_inf_req = requests.get(base_qry + join_qry)
            qry_info = qry_inf_req.json()
            if total_rows == 0:
                break
            elif total_rows == 1:
                total_rows = qry_info['response']['numFound']
                logger.debug(
                    f"{total_rows} images were found for query "
                    f"'{base_qry + join_qry}'")

            for img in qry_info["response"]["docs"]:
                fecha_foto = datetime.strptime(
                    img['fecha_foto'], params.solr_dateformat)
                images.append({
                    'date_captured':
                        fecha_foto.strftime(params.std_dateformat),
                    'url': params.host_imgs[self.collection] + img['ruta'],
                    'file_name': ntpath.split(img['ruta'])[-1],
                    'id': img['id'],
                    'width': 1.,
                    'height': 1.,
                    'location': img['nombre_congl']
                })
            start += params.rowspp
        return images

    def get_annotations(self, imgs_qry, anns_qry, bboxes=False):
        """Consult information from a data source from a set of images and
        annotations queries and build the information related to each
        annotation.

        Parameters
        ----------
        imgs_qry : str
          Solr images query
        anns_qry : str
          Solr annotations query
        bboxes : bool, optional
          Whether to create bounding box annotations or not (default is False)

        Returns
        -------
        List of dict
          List that contains the information of each annotation
        """
        total_rows = 1
        start = 0
        annotations = []
        while start < total_rows:
            base_qry = \
                params.solr_qry.format(
                    'anotaciones', params.rowspp, start, anns_qry)
            join_qry = params.join_coll.format(self.collection, imgs_qry)
            qry_inf_req = requests.get(base_qry + join_qry)
            qry_info = qry_inf_req.json()
            if total_rows == 0:
                break
            elif total_rows == 1:
                total_rows = qry_info['response']['numFound']
                logger.debug(
                    f"{total_rows} annotations were found for query "
                    f"'{anns_qry}'")

            for ann in qry_info["response"]["docs"]:
                annot = {
                    'item_id': ann['ftrampa_id'],
                    'id': ann['id']
                }
                if bboxes:
                    annot['bbox'] = [ann['rect_x'], ann['rect_y'],
                                     ann['rect_width'], ann['rect_height']]
                if ann['tipo_anotacion'] == 'especimen':
                    annot['category_id'] = ann['taxa_id']
                else:
                    annot['category_id'] = '0'
                annotations.append(annot)
            start += params.rowspp
        return annotations

    def get_categories(self, imgs_qry, anns_qry):
        """Consult information from a data source from a set of images and
        annotations queries and build the information related to each category.

        Parameters
        ----------
        imgs_qry : str
          Solr images query
        anns_qry : str
          Solr annotations query

        Returns
        -------
        List of dict
          List that contains the information of each category
        """
        base_qry = params.solr_qry.format('anotaciones', 0, 0, anns_qry)
        facet_qry = params.fct_fld_rq.format('taxa_id')
        join_qry = params.join_coll.format(self.collection, imgs_qry)
        taxa_query = base_qry + facet_qry + join_qry
        qry_inf_req = requests.get(taxa_query)
        qry_info = qry_inf_req.json()
        taxa_ids = qry_info['facet_counts']['facet_fields']['taxa_id'][0::2]
        logger.debug(
            f"{len(taxa_ids)} taxa registers were found for query "
            f"'{taxa_query}'")
        taxa_qry = f"id:({' '.join(taxa_ids)})"
        base_qry = params.solr_qry.format('taxonomia', 10000, 0, taxa_qry)
        extra_qry = '&q.op=OR&fl=ascendentes_oblig'
        r = requests.get(base_qry + extra_qry)
        logger.debug(f"Query for categories: '{base_qry + extra_qry}'")
        taxa_info = r.json()
        for taxa in taxa_info['response']['docs']:
            asc_ind = taxa['ascendentes_oblig'].split(',')
            for taxa_lvl in params.taxa_lvls:
                index = [x for x in params.taxa_indexes[taxa_lvl]
                         if asc_ind[x]]
                if not index:
                    continue
                taxa_id = asc_ind[index[0]]
                if taxa_id not in taxa_ids:
                    taxa_ids.append(taxa_id)
        taxa_qry = f"categoria_taxonomica:* AND id:({' '.join(taxa_ids)})"
        base_qry = params.solr_qry.format('taxonomia', 10000, 0, taxa_qry)
        extra_qry = \
            '&q.op=OR&fl=id,name:nombre,category_path:ascendentes_oblig,'\
            'taxa_cat:categoria_taxonomica,taxa_subcat:subcategoria_taxonomica'
        r = requests.get(base_qry + extra_qry)
        taxa_info = r.json()
        categories = taxa_info['response']['docs']
        for category in categories:
            asc_ind = category['category_path'].split(',')
            ascendings = ['', '', '', '', '', '', '', '']
            len_asc = len(params.taxa_lvls)
            is_subsp = category['taxa_cat'] == 'especie' \
                and category.get('taxa_subcat', '') in params.taxa_subspcs
            if is_subsp:
                ascendings[-1] = str(category['id'])
            for i, taxa_lvl in enumerate(params.taxa_lvls, 1):
                index = [x for x in params.taxa_indexes[taxa_lvl]
                         if asc_ind[x]]
                if not index:
                    continue
                taxa_id = asc_ind[index[0]]
                ascendings[len_asc - i] = taxa_id
            category["category_path"] = ",".join(ascendings)
            del category["taxa_cat"]
            if "taxa_subcat" in category:
                del category["taxa_subcat"]
        return categories

    def get_names_and_is_a(self):
        """Consult information of categories from a data source and build both
        `is_a` and `dictionary` files.

        Returns
        -------
        dict, dict
          Two dictionaries: one with name definitions and one with the
          relation of each category with its parent.
        """
        taxa_qry = f"categoria_taxonomica:({' '.join(params.taxa_lvls)})"
        extra_qry = '&q.op=OR&fl=id,id_asc:id_asc_oblig,name:nombre_str'
        total_rows = 1
        start = 0
        rowspp = 10000
        names = {}
        is_a = {}
        while start < total_rows:
            base_qry = params.solr_qry.format(
                'taxonomia', rowspp, start, taxa_qry)
            r = requests.get(base_qry + extra_qry)
            taxa_info = r.json()
            if total_rows == 1:
                total_rows = taxa_info['response']['numFound']
                logger.debug(f"{total_rows} registers for query "
                             f"'{base_qry + extra_qry}'")
            categories = taxa_info['response']['docs']
            for taxa in categories:
                names[taxa['id']] = taxa['name']
                is_a[taxa['id']] = taxa['id_asc']
            start += rowspp
        return (names, is_a)


def build_snmb(**kwargs):
    year = kwargs.get("year", 2019)
    destination_path = kwargs.get("destination_path", "./snmb_dataset")
    description = kwargs.get("description", params.snmb_description)
    version = kwargs.get("version", "1.0")

    ds_snmb = DataSourceBuilder(
        "snmb", year, destination_path, description, version)
    return ds_snmb.build()


def build(collection="snmb", **kwargs):
    res = {
        'snmb': lambda x: build_snmb(**x)
    }.get(collection, "ERR")(kwargs)
    return res
