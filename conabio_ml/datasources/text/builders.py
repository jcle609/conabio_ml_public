
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

import datetime
import json
import multiprocessing
import time
import os
import pydash
import re
import urllib
import urllib.request

from multiprocessing import Pool, Manager, Value

from ...utils.logger import get_logger

logger = get_logger(__name__)


class SolrPost():
    """
    Auxiliar class to get dataset items from Solr instance 
    """

    def __init__(self, solr_url):
        """
        Parameters
        ----------
        solr_url : str
            url address of solr instance
        """
        self.solr_url = solr_url \
            if solr_url.endswith("/") \
            else solr_url + "/"

    def __fix_name(self, name):
        if " " in name:
            name = name.replace(" ", "%20")

        name = re.sub('[\x80-\xFF]', lambda c: '%%%02x' %
                      ord(c.group(0)), name)

        return name

    def select(self, solr_core, query="q=*:*"):
        """
        Retrieve results from solr instance according to the query 

        Attributes
        ----------
        solr_core : string
            Name of the instance in solr to query
        query: string
            Query to apply to solr instance, at least param q must to be set
        """
        try:
            query = self.__fix_name(query)

            solr_core = solr_core \
                if solr_core.endswith("/") \
                else solr_core + "/"

            request = urllib.request.Request(
                url=self.solr_url + solr_core + "select?" + query + "&wt=json")
            response = urllib.request.urlopen(request)
            result = json.loads(response.read().decode('utf-8'))

            return result
        except Exception as ex:
            logger.exception(f"Could not retrieve solr query {query}")
            logger.exception(ex)


class MWS():
    """
    Class to build the dataset for MWS in the format established in
    https://ecoinformatica.atlassian.net/wiki/spaces/CONML/pages/308838436/Fuentes+de+datos

    Attributes
    ----------
    dataset : dict
        Resulting dataset to store in json format
    data: dict
        Collected data in raw format
    manager: multiprocess.Manager
        Manager to handle parallel access to solr instance
    shared_cat_id: multiprocess.Dict
        Ids of the categories of the dataset
    shared_cats: multiprocess.Dict
        Categories of the dataset
    shared_items: multiprocess.Dict
        Items of the dataset
    shared_anns: multiprocess.Dict
        Annotations of the dataset
    descriptor: dict
        Info of the dataset 
    """

    dataset = {}
    data = {}
    manager = Manager()
    shared_cat_id = manager.Value('i', 1)
    shared_cats = manager.dict()
    shared_items = manager.dict()
    shared_anns = manager.dict()

    batch_size = 500

    descriptor = {
        "description": "Dataset de mineria web services",
        "version": "1.0",
        "date_created": datetime.datetime.now().strftime("%Y-%m-%d"),
        "year": datetime.datetime.now().strftime("%Y"),
        "url": "ftp://172.16.3.111/share/corpora/text/mws_2019_1.0.tar.gz",
        "contributor": "Coordinación de Ecoinformática",
        "type": "json"
    }

    def __init__(self, destination_path, solr_url, solr_core):
        """
        Parameters
        ----------
        post : SolrPost 
            Instance for the auxiliar class to retrieve results
        solr_core : dict
            Core to query for results
        destination_path: string
            Path where json dataset will be stored
        found: multiprocess.Dict
            Auxiliar dict to distinguish repeated results
        """
        self.post = SolrPost(solr_url)
        self.solr_core = solr_core
        self.destination_path = destination_path
        self.found = self.manager.dict()

    def create_pool(self, query, term):
        """
        Creates the pool of queries to handle in parallel

        Parameters
        ----------
        query : str
            Query to search all results
        term : str
            Temr to search

        Returns
        -------
        [dict]
        Batches to search in parallel, contains
        {start: start row, 
         total: total rows, 
         term: term to search} 
        """

        all_batches = []
        results = self.post.select(self.solr_core, query)
        num_found = results["response"]["numFound"]
        batches = [{"start": r, "total": num_found, "term": term}
                   for r in range(0, num_found, self.batch_size)]
        all_batches += batches

        return all_batches

    def collect(self, input):
        """
        Collects of a single batch query

        Parameters
        ----------
        input : dict
            Params to search, contains:
            start: start row to search
            num_found = total rows
            term = term to search
            query = query to send to solr instance
            label = label that corresponds to the results

        Returns
        -------
        List of dict
            List of rows resulting of querying the instance
        """
        res = []
        start = input["start"]
        num_found = input["total"]
        term = input["term"]
        query = input["query"]
        label = input["label"]

        query = query.replace("{{2}}", str(self.batch_size))\
            .replace("{{3}}", str(start))

        results = self.post.select(self.solr_core, query)
        docs = results["response"]["docs"]

        for doc in docs:
            doc["label"] = label

            res.append(
                doc
            )

        return res

    def get_results(self, query, fields, label, processes=4):
        """
        Creates the list of batch queries and executes them
        then returns the results in a single dict

        Parameters
        ----------
        query : str
            Query to distribuite in batch queries
        fields : str
            Fields to retrieve for each row
        label : str
            Label corresponding to all results
        processes : int, optional
            Number of processes to parallelize

        Returns
        -------
        List of dict
            Result row for the searching
        """
        pool_query = query + "&fl=id"
        data = self.create_pool(pool_query, "*")

        for d in data:
            d["query"] = query + "&rows={{2}}&start={{3}}"
            d["label"] = label

        pool = multiprocessing.Pool(processes=processes)
        results = pool.map(self.collect, data)
        pool.close()
        pool.join()

        return pydash.chain(results).flatten().value()

    def collect_results(self, aux_file=None):
        """
        Reads the terms and builds the types of query for each case
        and stores on self.data property

        Parameters
        ----------
        aux_file : string
            File path where terms to search are
        """

        if aux_file:
            with open(aux_file) as terms_file:
                lines = terms_file.read()
                terms = lines.lower().split("\n")

            for term in terms:
                if len(term.strip()) > 0:
                    temp = self.get_results("q=open_search:\""+term+"\" AND tag:p",
                                            ["id", "open_search", "ordinal"],
                                            ["terms"])

                    for t in temp:
                        id = t["id"]

                        if id in self.data:
                            if t["label"][0] not in self.data[id]["label"]:
                                self.data[id]["label"].append(t["label"][0])
                        else:
                            self.data[id] = t
        else:
            temp = self.get_results("q=species:* AND tag:p",
                                    ["id", "open_search", "ordinal"],
                                    ["species"])

            for t in temp:
                id = t["id"]
                self.data[id] = t

    def __apply(self, func):
        """
        Auxiliar function to parallelize json transformation of
        retrieved results

        Parameters
        ----------
        destination_folder : str
        The folder path where the dataset will be stored.
        json_file : str
        Path of a json file that will be converted into a Dataset.
        categories : list of str, optional
        List of categories to filter registers (default is [])
        **kwargs :
        Extra named arguments passed to build_json

        Returns
        -------
        Dataset
        Instance of the created Dataset
        """

        pool = Pool(processes=4)
        pool.map(func, self.data)
        pool.close()
        pool.join()

    def transform_to_json(self, id):
        """
        Transforms each row in shared_items property
        to the dataset representation defined in
        https://ecoinformatica.atlassian.net/wiki/spaces/CONML/pages/308838436/Fuentes+de+datos


        Parameters
        ----------
        id : str
            ID of the row
        """

        data = self.data[id]

        id = data["id"]
        cats = data["label"]

        self.shared_items[id] = {
            "id": id,
            "file_id": data["file_id"][0],
            "data": data["content"][0],
            "ordinal": data["ordinal"],
            "source": data["source"],
            "title": data["title"][0]
        }

        for cat in cats:
            ann_id = id + "_" + cat
            if cat not in self.shared_cats:
                self.shared_cats[cat] = {
                    "id": self.shared_cat_id.value,
                    "name": cat
                }
                self.shared_cat_id.value += 1

            id_cat = None
            for cat_found in dict(self.shared_cats):
                if cat_found == cat:
                    id_cat = self.shared_cats[cat_found]["id"]

            if ann_id not in self.shared_anns:
                self.shared_anns[ann_id] = {
                    "id": id,
                    "item_id": id,
                    "class": id_cat
                }

    def transform(self):
        """
        Transforms raw results of search in self.data
        to representation of json dataset
        https://ecoinformatica.atlassian.net/wiki/spaces/CONML/pages/308838436/Fuentes+de+datos
        then stores it in destination_path
        """

        self.__apply(self.transform_to_json)

        cats = dict(self.shared_cats)
        items = dict(self.shared_items)
        anns = dict(self.shared_anns)

        self.dataset["info"] = self.descriptor
        self.dataset["categories"] = [c for c in cats.values()]
        self.dataset["items"] = [i for i in items.values()]
        self.dataset["annotations"] = [a for a in anns.values()]

        dataset_name = "mws_" + \
            self.descriptor["year"]+"_" +\
            self.descriptor["version"]+".json"

        with open(os.path.join(self.destination_path, dataset_name), 'w') as outfile:
            json.dump(self.dataset, outfile)


def build_mws(destination_path, terms_file, solr_url, solr_core):
    """
    Builds the dataset of MWS instance

    Parameters
    ----------
    destination_folder : str
        The folder path where the dataset will be stored.
    terms_file : str
        Path to file where terms to search are
    solr_url : str
        url of solr instance to query
    solr_core : str
        core of solr
    """
    mws = MWS(destination_path, solr_url, solr_core)
    mws.collect_results()
    mws.collect_results(terms_file)
    mws.transform()
