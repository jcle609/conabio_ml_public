import os
import pandas
import pydash
import unittest

from conabio_ml.datasets.collections import Collection
from examples import text as text_example
from examples import images as image_example


class DatasetTest(unittest.TestCase):
    test_path = "./examples/test"
    test_images_path = os.path.join(test_path, 'images')
    test_text_path = os.path.join(test_path, 'text')
    image_test_collection = None
    text_test_collection = None

    def setUp(self):
        try:
            collections = Collection.list(Collection.TYPES.Images)
            self.image_test_collection = collections[0]

            collections = Collection.list(Collection.TYPES.Text)
            self.text_test_collection = pydash.chain(collections)\
                .filter(lambda x: x["id"] == "mws")\
                .value()[0]

            print(self.text_test_collection)
        except TimeoutError as err:
            self.fail(err)

    def test_text_as_dataframe(self):
        self.assertNotEqual(self.text_test_collection, None)
        dataset_file = text_example\
            .load_json_collection(self.text_test_collection,
                                  self.test_text_path)
        self.assertTrue(os.path.isfile(dataset_file))

        dataset = text_example\
            .get_dataset_from_json(dataset_file)\
            .as_dataframe(columns=["item", "label"])

        self.assertEqual(type(dataset), pandas.DataFrame)
        print(dataset.head(5))

    def test_text_to_csv(self):
        to_csv_path = os.path.join(self.test_text_path, "to_csv")
        self.assertNotEqual(self.text_test_collection, None)
        dataset_file = text_example\
            .load_json_collection(self.text_test_collection,
                                  self.test_text_path)
        self.assertTrue(os.path.isfile(dataset_file))

        text_example.get_dataset_from_json(dataset_file)\
            .cross_category_split(train_cats=[2], test_cats=[1])\
            .to_csv(to_csv_path, columns=["item", "label"])

        self.assertTrue(os.path.isfile(
            os.path.join(to_csv_path, "dataset.csv")))

    def test_text_to_folder(self):
        self.assertNotEqual(self.text_test_collection, None)
        dataset_file = text_example\
            .load_json_collection(self.text_test_collection,
                                  self.test_text_path)
        self.assertTrue(os.path.isfile(dataset_file))

        text_example.get_dataset_from_json(dataset_file)\
            .split()\
            .to_folder(os.path.join(self.test_text_path, "to_folder"))

        self.assertTrue(os.path.isdir(
            os.path.join(self.test_text_path, "to_folder", "train")))
        self.assertTrue(os.path.isdir(
            os.path.join(self.test_text_path, "to_folder", "test")))

    def test_imagejson_loading(self):
        self.assertIsNotNone(self.image_test_collection)

        dataset_file = image_example.load_json_collection(
            self.image_test_collection, self.test_images_path)

        self.assertTrue(os.path.isfile(dataset_file))

    def test_image_as_dataframe(self):
        self.assertIsNotNone(self.image_test_collection)

        dataset_file = image_example.load_json_collection(
            self.image_test_collection, self.test_images_path)
        snmb_dataset = image_example.get_dataset_from_json(
            self.test_images_path, dataset_file)

        dfs = snmb_dataset.as_dataframe(
            columns=["item", "label", "bbox"],
            splitted=True)
        self.assertIsNotNone(dfs)

        df = snmb_dataset.as_dataframe(
            columns=["item", "label", "bbox"],
            splitted=False)
        self.assertIsNotNone(df)

    def test_image_to_csv(self):
        self.assertIsNotNone(self.image_test_collection)

        dataset_file = image_example.load_json_collection(
            self.image_test_collection, self.test_images_path)
        snmb_dataset = image_example.get_dataset_from_json(
            self.test_images_path, dataset_file)

        path_splt = os.path.join(self.test_images_path, "to_csv", "splitted")
        csv_files = snmb_dataset.to_csv(
            dest=path_splt,
            columns=["item", "label", "bbox"],
            header=False,
            splitted=True)
        for csv_file in csv_files:
            self.assertTrue(os.path.isfile(csv_file))

        path_no_splt = os.path.join(
            self.test_images_path, "to_csv", "no_splitted")
        csv_file = snmb_dataset.to_csv(
            dest=path_no_splt,
            columns=["item", "label"],
            header=True,
            splitted=False)
        self.assertTrue(os.path.isfile(csv_file))

    def test_image_to_folder(self):
        self.assertNotEqual(self.image_test_collection, None)

        dataset_file = image_example.load_json_collection(
            self.image_test_collection, self.test_images_path)
        snmb_dataset = image_example.get_dataset_from_json(
            self.test_images_path, dataset_file)

        to_folder_path = os.path.join(self.test_images_path, "to_folder")
        snmb_dataset.to_folder(
            path=to_folder_path,
            split_in_partitions=True,
            split_in_labels=True,
            keep_originals=True)
        for partition in snmb_dataset.params["partitions"]:
            self.assertTrue(os.path.isdir(
                os.path.join(to_folder_path, partition)))


if __name__ == '__main__':
    unittest.main()
