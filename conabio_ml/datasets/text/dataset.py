#!/usr/bin/env python
# -*- coding: utf-8 -*-
import copy
import json
import os
import numpy
import pandas
import pydash
import time
import threading
import uuid

from .. import dataset
from abc import ABCMeta, abstractmethod


class Dataset(dataset.Dataset):
    """ Dataset of type Text contains functionality only used
        in NLP tasks """

    __lock = threading.Lock()

    @classmethod
    def fetch_items(cls):
        raise Exception("Method not implemented")

    @classmethod
    def build_json(cls, data, categories, group_fields, multilabel) -> ({}, pandas.DataFrame):
        raise Exception("Method not implemented")

    def cross_category_split(self, train_cats=[], test_cats=[], val_cats=[]):
        """Split the dataset between `train`, `test` and `validation`
        partitions using sent categories in each partition

        If categories sum more than dataset items a WARNING appears

        Parameters
        ----------
        train_cats : list, optional
          Categories in train partition (default [])
        test_cats : list, optional
          Categories in test partition (default [])
        val_cats : list, optional
          Categories in val partition (default empty list)

        Returns
        -------
        Dataset
          Instance of the Dataset
        """
        partitions = {"train": train_cats,
                      "test": test_cats}
        if len(val_cats) > 0:
            partitions["val"] = val_cats

        are_cats_present = pydash.chain(partitions.values())\
            .flatten()\
            .map(lambda x: x in self.params["categories"])\
            .reduce(lambda x, y: x & y, True)\
            .value()

        assert are_cats_present, \
            "Some categories received are not present in global dataset categories"

        for part, cats in partitions.items():
            self.params["partitions"][part] =\
                self.data.index[self.data['label'].isin(cats)]

        total = pydash.chain(self.params["partitions"].values())\
            .map(lambda x: len(x))\
            .reduce(lambda x, y: x + y, 0)\
            .value()

        if total != self.data.shape[0]:
            print("WARNING: Items in splitted result set are not equal to dataset items ")

        return self

    def filesystem_writer(self, item, **kwargs):
        columns = ["id", "file_id", "label",
                   "ordinal", "partition", "item", "sopa"]
        dest_folder = kwargs["dest_folder"]
        item = item.to_dict()
        res = pydash.chain(columns)\
            .map(lambda x: (str(x)+":" + str(item[x]))
                 if item.get(x, None) is not None
                 else None)\
            .filter(lambda x: x is not None)\
            .reduce(lambda x, y: x + y + "\n", "")\
            .value()

        with self.__lock:
            id = item["id"] \
                if item.get("id", None) is not None \
                else str(uuid.uuid4())
            with open(os.path.join(dest_folder, id), mode="w") as _f:
                _f.write(res)


class MWS(Dataset):
    """ Since MWS is an specific scenario of Text Dataset it inherits from it, and loads
        all information defined in its JSON Dataset file """

    @classmethod
    def from_json(cls,
                  json_file,
                  categories=[]):
        """Creates an instance of MWS Dataset from a json file

        Parameters
        ----------
        destination_folder : str
          The location path where the dataset will be stored.
        json_file : str
          Path to a json file with the correct format to be converted
          to a Dataset object.
        categories : str, optional
          List of categories to filter registers (default is [])

        Returns
        -------
        Tuple
          (pandas.DataFrame, dict)
        """
        return super(MWS, cls).from_json(destination_folder="",
                                         json_file=json_file,
                                         categories=categories)

    @classmethod
    def fetch_items(cls):
        print("No need of fetching")

    @classmethod
    def build_json(cls,
                   data,
                   destination_folder,
                   categories,
                   group_fields=None,
                   multilabel=None) -> ({}, pandas.DataFrame):
        """ Builds a filtered dataset from the data loads from json file 
          and retrieves it as dataframa

        Parameters
        ----------
        data : json
          Data in json file
        destination_folder : str
          Destination folder to stores auxiliar data
        categories : list
          List of categories to filter

        Returns
        -------
        Tuple
          (pandas.DataFrame, dict)
        """
        anns = data["annotations"]
        items = data["items"]

        items_to_anns = {}

        for key, ann in anns.items():
            item_id = ann["item_id"]

            if item_id not in items_to_anns:
                items_to_anns[item_id] = []

            items_to_anns[item_id].append(ann)

        df_data = pydash.chain(items_to_anns.values())\
            .map(lambda x:  [{"item": items[x[0]["item_id"]]["data"],
                              "label": xx["category_id"],
                              "id":xx["id"],
                              "file_id":items[x[0]["item_id"]]["file_id"],
                              "ordinal": items[x[0]["item_id"]]["ordinal"]}
                             for xx in x]) \
            .flatten()

        if len(categories) > 0:
            df_data = df_data\
                .filter(lambda x: x["label"] in categories)

        return pandas.DataFrame(df_data.value()), data["info"]
