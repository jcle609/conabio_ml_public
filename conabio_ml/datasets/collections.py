#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

import json
import os
import pydash
import urllib.request
import tarfile

from datetime import datetime
from ftplib import FTP
from enum import Enum
from ..utils.logger import get_logger

logger = get_logger(__name__)


class Collection:
    """
    Obtains Dataset JSON files and provides simple functionality
    for searching those JSON files.

    Attributes
    ----------
    TYPES : enum
      avalible types of dataset 
    Text : obj
      provider for Text datasets
    Images : obj
      provider for Images datasets
    address : str
      address of server where collections are stored
    """
    address = "172.16.3.111"

    class TYPES(Enum):
        """
        Allowed types of collections
        """
        Text = "text"
        Images = "images"

    class __Loader:
        """
        Base class used by an specific type of Dataset 
        """

        class DatasetListReader:
            """
            Reader used to retrieve files of FTP server
            """

            def __init__(self):
                self.data = None

            def __call__(self, s):
                temp = s.decode("utf-8")
                self.data = json.loads(temp)

        def get_file(self,
                     destination_folder,
                     collection_type,
                     year,
                     version,
                     collection) -> str:
            """ It checks if Dataset file is in destination_folder, and retrieves filepath
                if not: tries to find tar file
                if tar not fount: tries to download from server

            Parameters
            ----------
            destination_folder : str
              Destination path to search Dataset file
            collection_type : str
              Type of collection to search, possible options: text/images
            year : int
              Year of collection
            version : str
              Version of collection
            collection : str
              Collection ID in server

            Returns
            -------
            str
              Path in filesystem to Dataset JSON file
            """
            try:
                collection_id = collection + "_" + \
                    str(year) + "_" +\
                    str(version)

                json_file = os.path.join(
                    destination_folder, collection_id + ".json")
                tar_file = os.path.join(
                    destination_folder, collection_id + ".tar.gz")

                # SCENARIOS
                if os.path.exists(json_file):
                    return json_file
                elif os.path.exists(tar_file):
                    if self.untar(collection_id=collection_id,
                                  destination_folder=destination_folder):
                        return self.get_file(destination_folder, collection_type,
                                             year, version, collection)
                else:
                    if self.download_collection(collection=collection,
                                                collection_type=collection_type,
                                                year=year,
                                                version=version,
                                                destination_folder=destination_folder):

                        return self.get_file(destination_folder, collection_type,
                                             year, version, collection)

            except:
                return None

        def download_collection(self,
                                collection,
                                collection_type,
                                year,
                                version,
                                destination_folder) -> bool:
            """Checks the availability of collection in server and tries to retreive it

            Parameters
            ----------
            collection : str
              ID of collection in server
            collection_type : str
              Type of collection. text/images 
            year : int
              Year of collection
            version : str
              Version of collection
            destination_folder : str
              Folder to store collection if found

            Returns
            -------
            bool
            True if the process was succesful

            """
            try:
                collection_id = collection + "_" + \
                    str(year) + "_" + str(version)

                datasets = self.get_dataset_list().get(collection)

                if not datasets:
                    raise Exception("Dataset " + collection
                                    + " not found")

                for d in datasets:
                    if str(d["year"]) == str(year) and str(d["version"]) == str(version):
                        dataset = d["path"]
                        break

                if dataset:
                    print("Trying to download ", collection_id)
                    return self.get_dataset_file(
                        os.path.join(collection_type,
                                     collection_id + ".tar.gz"),
                        os.path.join(destination_folder))
                else:
                    raise Exception("Dataset " + collection_id +
                                    "version" + year + "_"+version +
                                    " not found")

                return True
            except TimeoutError as ex:
                logger.exception(f"Timeout exception, please check your internet"
                                 f"connection")
                logger.exception(ex)
                raise ex
            except Exception as ex:
                logger.exception(ex)
                raise ex

        def get_dataset_list(self) -> dict:
            """
            Obtains the avilable collections in server

            Returns
            -------
            dict
            """
            print("Obteniendo lista de conjunto de datos")

            try:
                ftp = FTP("172.16.3.111")
                ftp.login()
                r = self.DatasetListReader()

                ftp.retrbinary('RETR /datasets.json', r)
                res = r.data

                ftp.close()

                return res
            except:
                raise

        def get_dataset_file(self,
                             filepath,
                             destination_path) -> bool:
            """
            Checks the Dataset file in destination path
              if not exists: downloads from server

            Parameters
            ----------
            filepath : str
              Path to search Dataset file
            destination_path : str
              If not found: path to store the retireved collection

            Returns
            -------
            bool 
              True if the process was succesful
            """
            try:
                id = "/".join(filepath.split("/")[1:])
                self.download(filepath,
                              destination_path=destination_path)

                dataset_filename = os.path.join(destination_path, id)

                if not os.path.isfile(dataset_filename):
                    print("EL ARCHIVO NO PUDO SER DESCARGADO")
                    return False

                return True
            except:
                print("EL ARCHIVO NO PUDO SER DESCARGADO")
                raise

        def download(self,
                     dataset,
                     destination_path,
                     attempts=1):
            """
            Downloads the Dataset file from server

            Parameters
            ----------
            dataset : str
              Dataset name
            destination_path : str
              Destination path to store
            attempts : int, optional
              Checks the quantity of attemps for accessing the server
            """
            print("El conjunto de datos "+dataset+" no existe")

            try:
                id = "/".join(dataset.split("/")[1:])
                print("Intentando descargar: "+dataset)
                data_path = os.path.join(destination_path, id)
                urllib.request.urlretrieve(
                    'ftp://172.16.3.111/' + dataset, data_path)

                print("Archivo descargado")
            except urllib.error.URLError as exc:
                if "530" in exc.reason and attempts < 3:
                    attempts += 1
                    print("Credenciales incorrectas")
                    self.download(dataset, destination_path, attempts)
                else:
                    print("Demasiados intentos fallidos")
                    input()
                    raise
            except:
                raise

        def untar(self,
                  collection_id,
                  destination_folder) -> bool:
            """ Check tar file is found, and if found untars it
              in destination folder

            Parameters
            ----------
            collection_id : str
              Collection file to search
            destination_folder : str
              Destination folder to untars the file

            Returns
            -------
            bool
              True if the process was succesful
            """
            try:
                print("Trying to uncompress ", collection_id)

                tar_file = os.path.join(destination_folder,
                                        collection_id + ".tar.gz")
                tar = tarfile.open(tar_file)

                for member in tar.getmembers():
                    try:
                        member.name = member.name[2:] \
                            if member.name.startswith("./")\
                            else member.name
                        temp_name = "/".join(member.name.split("/")[1:])

                        target_name = temp_name
                        member.name = os.path.join(target_name)
                        tar.extract(member, destination_folder)
                    except:
                        pass
                tar.close()

                print("TAR file decompressed... Removing TAR file ")
                os.remove(tar_file)

                return True
            except Exception as ex:
                logger.exception(f"There is an error decompressing TAR file")
                logger.exception(ex)
                raise ex

    class Text(__Loader):
        """
        Provides functionality related to Text datasets
        """

        @classmethod
        def fetch(cls,
                  destination_folder,
                  year=datetime.now().year,
                  version="1.0",
                  collection='mws') -> str:
            """ Retrieves the Dataset file of type Text and returns its filepath

            Parameters
            ----------
            destination_folder : str
              Destination path to search Dataset file
            year : int, optional
              Year of collection
            version : str, optional
              Version of collection
            collection : str, optional
              Collection ID in server

            Returns
            -------
            str
              Path in filesystem to Dataset JSON file
            """
            dataset_file = cls().get_file(destination_folder, "text",
                                          year, version, collection)

            return dataset_file

    class Images(__Loader):
        """
        Provides functionality related to Images datasets
        """

        @classmethod
        def fetch(cls,
                  destination_folder,
                  year=datetime.now().year,
                  version=1.0,
                  collection='snmb') -> str:
            """ Retrieves the Dataset file of type Images and returns its filepath

            Parameters
            ----------
            destination_folder : str
              Destination path to search Dataset file
            year : int, optional
              Year of collection
            version : str, optional
              Version of collection
            collection : str, optional
              Collection ID in server

            Returns
            -------
            str
              Path in filesystem to Dataset JSON file
            """
            dataset_file = cls().get_file(destination_folder, "images",
                                          year, version, collection)

            return dataset_file

    @classmethod
    def list(cls, type_collection) -> list:
        """
        Lists the collections available in server by type

        Parameters
        ----------
        type_collection : Collections.TYPES
          Type of collection to enlist 

        Returns
        -------
        list(dict)
        """

        assert type(type_collection) == Collection.TYPES,\
            "Incorrect type of collection. See Collection.Types"

        collection_list = cls.__Loader().get_dataset_list()
        collections = pydash.chain(collection_list.items())\
            .filter(lambda x: any(
                [xx["path"].split("/")[0] ==
                 type_collection.value for xx in x[1]]
            ))\
            .map(lambda x:  [{
                "version": xx["version"],
                "year": xx["year"],
                "path": xx["path"],
                "info": xx["info"],
                "id": x[0]
            } for xx in x[1]])\
            .flatten()\
            .value()

        return collections
