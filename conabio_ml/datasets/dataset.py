#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import json
import numpy
import os
import pandas
import pydash
import time
import tarfile
import threading
import uuid
import random

from abc import ABCMeta, abstractmethod
from datetime import datetime
from sklearn import model_selection

from ..utils.dataset_utils import get_all_files
from ..utils.logger import get_logger

logger = get_logger(__name__)
lock = threading.Lock()

REQUIRED_JSON_FIELDS = ['info', 'items', 'annotations', 'categories']

class Dataset():
    """Represents a Dataset specification.

    Attributes
    ----------
    data : pandas.DataFrame
      DataFrame object that holds dataset elements and contains at least
      the following columns:
        * item
        * label
    info: dict
      Dictionary that contains dataset information
    params: dict
      Dictionary that contains configuration and state parameters of the
      dataset
    """

    REQUIRED_COLUMNS = ["item", "label"]
    def __init__(self, data, info):
        """
        Parameters
        ----------
        data : pandas.DataFrame
          DataFrame object that make up the dataset.
        info : dict
          Information of the dataset.
        """
        # Check standard columns
        self.__check_dataframe(data), "Malformed dataframe data"

        logger.debug(
            f"Creating dataset with {len(data)} registers and info: '{info}'")

        self.params = {
            "categories": [cat for cat in data["label"].unique()],
            "partitions": {}
        }

        logger.debug(f"Categories of dataset: {self.params['categories']}")

        self.info = info
        self.data = data

    """
  CLASS METHODS FROM
  """
    @classmethod
    def from_json(cls,
                  destination_folder,
                  json_file,
                  categories=[],
                  **kwargs):
        """Creates a Dataset from a json file.

        Parameters
        ----------
        destination_folder : str
          The folder path where the dataset will be stored.
        json_file : str
          Path of a json file that will be converted into a Dataset.
        categories : list of str, optional
          List of categories to filter registers (default is [])
        **kwargs :
          Extra named arguments passed to build_json

        Returns
        -------
        Dataset
          Instance of the created Dataset
        """
        logger.info(
            f"Creating dataset from json file: {os.path.abspath(json_file)}")
        logger.info(f"Dataset categories: {categories}")
        logger.info(f"Loading annotations from json file into memory...")
        tic = time.time()
        assert json_file is not None and os.path.isfile(json_file), \
            "json_file of collection info is not correct"

        json_data = json.load(open(json_file, 'r'))
        assert cls.__check_json(json_data), \
            f"Annotation file format {type(json_data)} not supported"
        data, info = cls.build_json(
            json_data, destination_folder, categories, **kwargs)
        logger.info('Done (t={:0.2f}s)'.format(time.time() - tic))
        return cls(data, info)

    @classmethod
    def from_csv(cls,
                 csv_source,
                 columns=None,
                 header=True,
                 column_mapping=None,
                 split_by_filenames=False,
                 split_by_column=None,
                 info={}):
        """Creates a Dataset from a csv file.

        Parameters
        ----------
        csv_source : str
          Path of a csv file or a folder containing csv files that will be
          converted into a Dataset.
          The csv file(s) must have at least two columns, which represents
          `item` and `label` data.
        columns : list of str, list of int, or None, optional
          List of column names, or list of column indexes, to load from the
          csv file(s). If None, load all columns. (default is None)
        header : bool, optional
          Whether or not csv contains header at row 0 (default is True).
          If False, you need to map at least `item` and `label` columns by
          position in column_mapping parameter.
        column_mapping : dict, optional
          Dictionary to map column names in dataset.
          If header=True mapping must be done by the column names,
          otherwise must be done by column positions.
          E.g.::
            header=True: column_mapping={"c1": "item", "c2": "label"}
            header=False: column_mapping={0: "item", 1: "label"}
          (default is None)
        split_by_filenames : bool, optional
          If `csv_source` is the path of a folder containing csv files,
          splits the dataset by the file names. (default is False).
          The file names must be the following::
            `train.csv` for `train` partition
            `test.csv` for `test` partition
            `validation.csv` for `validation` partition
        split_by_column : str or int, optional
          Column name or column index in the csv file(s) to split the dataset
          (default is None).
          The values in the column must be the following::
            `train` for `train` partition
            `test` for `test` partition
            `validation` for `validation` partition
        info : dict, optional
          Information of the csv file (default is {})

        Returns
        -------
        Dataset
          Instance of the created Dataset
        """
        valid_files = ["train.csv", "test.csv", "validation.csv"]
        isdir = os.path.isdir(csv_source)
        if split_by_column and columns and split_by_column not in columns:
            columns.append(split_by_column)
        header = 0 if header is True else None

        if isdir:
            logger.info(
                f"Creating dataset from CSV files in folder: "
                f"{os.path.abspath(csv_source)}")
            columns_buffer = None
            csvs = get_all_files(csv_source, seek_extension=".csv")
            assert len(csvs) > 0, \
                f"Folder {csv_source} does not contain valid csv files"
            df = pandas.DataFrame()
            for csv_file in csvs:
                if split_by_filenames and csv_file["title"] not in valid_files:
                    logger.info(
                        f"{csv_file['title']} is not a valid CSV "
                        f"name for a partition. Skipped")
                    continue
                csv_path = os.path.join(csv_file["path"], csv_file["title"])
                logger.info(f"Reading data from CSV file {csv_path}")
                temp = pandas.read_csv(
                    csv_path, header=header, usecols=columns)
                if split_by_filenames:
                    part_name = csv_file["title"].split('.csv')[0]
                    temp["partition"] = part_name
                    logger.info(f"Data loaded for partition {part_name}")

                if not columns_buffer:
                    columns_buffer = [c for c in temp.columns]
                else:
                    def check_columns(cols_1, cols_2):
                        if len(cols_1) != len(cols_2):
                            return False
                        check = pydash.chain(cols_1)\
                            .map(lambda x: x in cols_2)\
                            .reduce(lambda x, y: x & y, True)\
                            .value()
                        return check

                    assert check_columns(temp.columns, columns_buffer),\
                        f"CSV files in {csv_source} are not in the correct "\
                        f"column format"
                df = pandas.concat([df, temp], ignore_index=True)
        else:
            assert os.path.isfile(csv_source), \
                f"Source {csv_source} is not a valid file or folder"
            logger.info(
                f"Creating dataset from CSV file: "
                f"{os.path.abspath(csv_source)}")
            df = pandas.read_csv(csv_source, header=header, usecols=columns)

        logger.debug(f"Columns in dataset: {columns}")
        if column_mapping:
            found = pydash.chain(column_mapping.keys())\
                .map(lambda x: x in df.columns)\
                .reduce(lambda x, y: x & y, True)\
                .value()
            assert found, \
                "column_mapping could not be performed, column names are no "\
                "present in result DataFrame"

            df = df.rename(columns=column_mapping)

        instance = cls(df, info)
        if split_by_filenames:
            split_by_column = "partition"
        if split_by_column:
            instance.__split_by_column(
                column=split_by_column, delete_column=True)
        return instance

    @classmethod
    def from_folder(cls,
                    source_folder,
                    extensions=[""],
                    split_by_folder=True,
                    include_id=False,
                    info={}):
        """Creates a Dataset from a folder structure

        Parameters
        ----------
        source_folder : str
          Path of a folder to be loaded and converted into a Dataset object.
        extensions : list of str, optional
          List of extensions to seek files in folders.
          [""] to seek all files in folders (default is [""])
        split_by_folder : bool, optional
          Split the dataset from the directory structure (default is False).
          For this option, directories must be in the following structure::
            - source_folder
              - `train`
                - class folders
              - `test`
                - class folders
              - `validation`
                - class folders
        include_id : bool, optional
          Wheter or not to include an id for each register (default is False)
        info : dict, optional
          Information of the folder structure (default is {})

        Returns
        -------
        Dataset
          Instance of the created Dataset
        """
        logger.info(
            f"Creating dataset from folder: {os.path.abspath(source_folder)}")
        res = []
        source_folder = source_folder \
            if source_folder.endswith("/") \
            else source_folder + "/"

        extensions = list(set([x.lower() for x in extensions]))
        _files = []
        for ext in extensions:
            _files += get_all_files(source_folder, seek_extension=ext)

        logger.debug(
            f"{len(_files)} found files for extensions {source_folder}")
        for f in _files:
            path = f["path"].replace(source_folder, "")
            label = "/".join(path.split("/")[1:]) if split_by_folder else path
            temp = {
                "item": os.path.join(source_folder, path, f["title"]),
                "label": label
            }
            if include_id:
                temp["id"] = str(uuid.uuid4())
            if split_by_folder:
                partition = path.split("/")[0]
                if partition not in ['train', 'test', 'validation']:
                    raise Exception(
                        "Files have to be contained in folders called "
                        "[train, test, validation] for [split_by_folder] "
                        "option")
                temp["partition"] = partition
                logger.debug(f"Folder for partition {partition} was found")
            res.append(temp)

        data = pandas.DataFrame(res)
        instance = cls(data, info)
        if split_by_folder:
            instance.__split_by_column(column="partition", delete_column=True)
        return instance

    """
    AUXILIAR METHODS OF CLASS-OVERLOADS
    """
    @classmethod
    def __check_json(cls, data):
        if not type(data) == dict:
            logger.error(f"Data of type {type(data)} is not supported")
            return False
        return all([field in data for field in REQUIRED_JSON_FIELDS])

    def __split_by_column(self, column='partition', delete_column=True):
        logger.debug(f"Splitting dataset by column {column}")
        if column not in self.data:
            return
        partitions = self.data[column].unique()
        for partition in partitions:
            self.params["partitions"][partition] = numpy.array(
                self.data.loc[self.data["partition"] == partition].index)
        if delete_column:
            self.data.drop(column, axis=1, inplace=True)

    def __check_dataframe(self, data) -> bool:
        columns = [c for c in data.columns]

        logger.debug(f"Columns for data: {columns}")
        required_columns_found = pydash.chain(self.REQUIRED_COLUMNS)\
            .map(lambda x: x in columns)\
            .reduce(lambda x, y: x & y, True)\
            .value()
        assert required_columns_found, \
            f"data needs at least {self.REQUIRED_COLUMNS} columns"
        assert type(data) == pandas.DataFrame, 'data must be DataFrame type'

    """
    SPLIT METHODS
    """
    def __create_sequences(self, max_secs=2):
        # TODO: Generalize to any fields
        date_format = "%Y-%m-%d %H:%M:%S"
        imgs_seqs = {}
        last_location = ""
        last_date = None

        ordered_data = self.data.sort_values(['location', 'date_captured'])
        self.data["sequence_id"] = ""
        for index, row in ordered_data.iterrows():
            if row['location'] == last_location:
                d2 = datetime.strptime(row['date_captured'], date_format)
                d1 = datetime.strptime(last_date, date_format)
                sec_diff = (d2 - d1).total_seconds()
                if sec_diff > max_secs:
                    sequence = str(uuid.uuid4())
            else:
                sequence = str(uuid.uuid4())
            self.data.at[index, "sequence_id"] = sequence
            last_date = row['date_captured']
            last_location = row['location']

    def split(self, 
              train_perc=0.8, 
              test_perc=0.2, 
              val_perc=0, 
              max_secs_in_sequences=None):
        """Split the dataset between `train`, `test` and `validation`
        partitions.
        `train_perc`, `test_perc` and `val_perc` must sum to 1.

        Parameters
        ----------
        train_perc : float, optional
          Train set percentage. Should be between 0 and 1. (default is 0.8)
        test_perc : float, optional
          Test set percentage. Should be between 0 and 1. (default is 0.2)
        val_perc : float, optional
          Validation set percentage. Should be between 0 and 1. (default is 0)
        max_secs_in_sequences : int, optional
          Maximum number of seconds between each element of a location-time
          sequence. If None, items will not be grouped in sequences.
          (default is None)

        Returns
        -------
        Dataset
          Instance of the Dataset
        """
        perc_sum = train_perc + test_perc + val_perc
        assert perc_sum > 0.99 and perc_sum < 1.01, 'Partitions have to SUM 1'

        logger.info(
            f"Splitting dataset in train:{train_perc}, test:{test_perc} and "
            f"val:{val_perc}")
        if max_secs_in_sequences is not None:
            logger.info(f"max_secs_in_sequences:{max_secs_in_sequences}")
        grouped = max_secs_in_sequences is not None
        if grouped:
            self.__create_sequences(max_secs_in_sequences)

        partitions = {"train": train_perc,
                      "test": test_perc}
        if val_perc > 0.:
            partitions["validation"] = val_perc

        total = 0.
        len_unique_items = len(self.data['item'].unique())
        unique_elems = self.data['item'].unique() if not grouped else \
            self.data['sequence_id'].unique()
        ordinals_uniq_elems = numpy.array(range(0, len(unique_elems)))
        all_elems = self.data['item'].values if not grouped else \
            self.data['sequence_id'].values
        ordinals_of_elems = {}
        for i, elem in enumerate(all_elems):
            if elem not in ordinals_of_elems:
                ordinals_of_elems[elem] = []
            ordinals_of_elems[elem].append(i)

        def _split(data, size):
            if grouped:
                random.seed(42)
                random.shuffle(data)
                n_items = int(size * len_unique_items)
                n, i = 0, 0
                for x in data:
                    if n > n_items:
                        break
                    i += 1
                    n += len(ordinals_of_elems[unique_elems[x]])
                return data[i:], data[:i]
            else:
                return model_selection.train_test_split(
                    data, test_size=size, random_state=42)

        for part, val in partitions.items():
            if total + val == 1:
                self.params["partitions"][part] = numpy.array(
                    [y for x in ordinals_uniq_elems
                     for y in ordinals_of_elems[unique_elems[x]]])
            else:
                part_size = val/(1. - total)
                offset, partition = _split(ordinals_uniq_elems, part_size)
                self.params["partitions"][part] = numpy.array(
                    [y for x in partition for y in
                        ordinals_of_elems[unique_elems[x]]])
                ordinals_uniq_elems = offset
                total += val

        return self

    """
  AS METHODS
  """

    def as_dataframe(self, columns=None, splitted=False):
        """Gets a DataFrame representation of the Dataset.

        Parameters
        ----------
        columns : list of str, optional
          List of columns in the DataFrame (default is None)
        splitted : bool, optional
          Whether or not to split the dataset into `train`, `test` and
          `validation` DataFrames (default is False)

        Returns
        -------
        DataFrame or list of DataFrame
          DataFrame representing the Dataset.
          If `splitted` is True, returns a list of DataFrames.
        """
        is_splitted = len(self.params["partitions"].keys()) > 0

        df = None
        if columns:
            can_select = pydash.chain(columns)\
                .map(lambda x: x in self.data.columns)\
                .reduce(lambda x, y: x & y, True)\
                .value()

            if can_select:
                df = self.data[columns]
            else:
                logger.error(f"columns are not part of DataSet columns")
                raise Exception("columns are not part of DataSet columns")
        else:
            df = self.data

        if is_splitted:
            if splitted:
                temp = pydash.chain(self.params["partitions"])\
                    .map(lambda x: df.iloc[x])\
                    .value()
                df = temp
            else:
                df["partition"] = ""
                partitions = pydash.chain(self.params["partitions"].items())\
                    .value()
                  
                for partition, value in partitions:
                    df.loc[value, "partition"]=partition
        return df

    """
  TO METHODS
  """

    def to_folder(self,
                  path,
                  train_folder_name='train',
                  test_folder_name='test',
                  val_folder_name='validation',
                  split_in_partitions=True,
                  split_in_labels=True,
                  keep_originals=True):
        """Creates a filesystem representation of the dataset.

        Parameters
        ----------
        path : str
          Path where the dataset files will be stored
        train_folder_name : str, optional
          Name of the `train` folder (default is 'train')
        test_folder_name : str, optional
          Name of the `test` folder (default is 'test')
        val_folder_name : str, optional
          Name of the `validation` folder (default is 'validation')
        split_in_partitions : bool, optional
          Whether or not to split items in folders with partition names.
          Only works if the dataset has been previously splitted.
          (default is True)
        split_in_labels : bool, optional
          Whether or not to split items in folders with label names.
          These folders will be below the partition folders (default is True)
        keep_originals : bool, optional
          Whether to keep original files or delete them (default is True)
        """
        logger.info(f"Writting dataset in folder: {os.path.abspath(path)}")
        df = self.as_dataframe()

        folder_names = {
            'train': train_folder_name,
            'test': test_folder_name,
            'validation': val_folder_name
        }

        split_in_partitions = split_in_partitions and 'partition' in df.columns
        for _, row in df.iterrows():
            dest_folder = path
            if split_in_partitions:
                dest_folder = os.path.join(
                    dest_folder, folder_names[row["partition"]])

            if split_in_labels:
                dest_folder = os.path.join(dest_folder, str(row["label"]))

            if not os.path.exists(dest_folder):
                with lock:
                    os.makedirs(dest_folder)

            self.filesystem_writer(
                row, dest_folder=dest_folder, keep_originals=keep_originals)

    def to_csv(self,
               dest,
               columns=None,
               header=True,
               splitted=False):
        """Creates csv file(s) representing the dataset.

        Parameters
        ----------
        dest : str
          Path where the csv file(s) will be stored
        columns : list of str, optional
          List of columns to add in the csv (default is None)
        header : bool, optional
          Whether or not to add a header in the csv file (default is True)
        splitted : bool, optional
          Whether or not to split the dataset into `train`, `test` and
          `validation` csv files.
          Only works if the dataset has been previously splitted
          (default is False)

        Returns
        -------
        str or list of str
          Path or list of paths of csv file(s) created
        """
        logger.info(f"Writting dataset in CSV files: {dest}")
        is_dir = not dest.endswith('.csv')
        if is_dir and not os.path.exists(dest):
            os.makedirs(dest)
        if columns is None:  # Default columns
            columns = ["item", "label"]
            if self.info.get("dataset_type", "") == 'object_detection':
                columns.append("bbox")
        logger.debug(f"Columns of the dataset: {columns}")
        dfs = self.as_dataframe(columns=columns, splitted=splitted)
        if splitted:
            part_names = [x for x in self.params["partitions"].keys()]
            csv_names = []
            for i in range(len(dfs)):
                if is_dir:
                    csv_name = os.path.join(dest, f"{part_names[i]}.csv")
                else:
                    prefix = dest.split('.csv')[0] if dest.endswith(
                        '.csv') else dest
                    csv_name = f"{prefix}_{part_names[i]}.csv"
                logger.info(f"File {os.path.abspath(csv_name)} created")
                dfs[i].to_csv(csv_name, index=False, header=header)
                csv_names.append(csv_name)
            return csv_names
        else:
            if is_dir:
                csv_name = os.path.join(dest, "dataset.csv")
            else:
                csv_name = dest if dest.endswith('.csv') else f"{dest}.csv"
            logger.info(f"File {os.path.abspath(csv_name)} created")
            dfs.to_csv(csv_name, index=False, header=header)
            return csv_name

    """
    ABSTRACT METHODS
    """

    @abstractmethod
    def filesystem_writer(self, item, **kwargs):
        raise Exception("Method not implemented")

    @classmethod
    @abstractmethod
    def build_json(cls, data, destination_folder, categories, group_fields,
                   multilabel):
        raise Exception("Method not implemented")
