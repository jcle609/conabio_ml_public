#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import io
import json
import time
import math
import uuid
import pip
import subprocess
from datetime import datetime
from PIL import Image
import pandas as pd
from collections import defaultdict
import itertools
import contextlib2
from shutil import copy2

from ..dataset import Dataset
from ...utils.logger import get_logger
from ...utils.dataset_utils import parallel_download, is_array_like

logger = get_logger(__name__)


class ImageDataset(Dataset):
    """Represents a ImageDataset specification."""
    
    @classmethod
    def build_json(cls,
                   json_data,
                   destination_folder,
                   categories,
                   **kwargs):
        """Build DataFrame from a json file.

        Parameters
        ----------
        json_data : dict
          Dictionary that contains imformation stored in a json file.
        destination_folder : str
          The location path where the dataset will be stored.
        categories : list of str, optional
          List of categories to filter registers (default is [])
        **kwargs : 
          Extra named arguments that may contains the following parameters:
            * multilabel : bool, optional
                Whether to create a multilabel dataset (default is False)

        Returns
        -------
        (pandas.DataFrame, dict)
          Tuple of DataFrame object and info dict
        """
        multilabel = kwargs.get("multilabel", False)
        logger.debug(
            f"Dataset kwargs: \n"
            f"multilabel ({multilabel})")
        info = json_data["info"]
        json_handler = JsonHandler(json_data)
        cat_ids = json_handler.get_cat_ids(cat_nms=categories)
        ann_ids = json_handler.get_ann_ids(cat_ids=cat_ids, by_supercats=True)
        anns = json_handler.load_anns(ann_ids)
        items_ids = \
            json_handler.get_item_ids(cat_ids=cat_ids, by_supercats=True)
        items = json_handler.load_items(items_ids)
        images_folder = os.path.join(destination_folder, 'data')
        json_handler.download(images_folder, items_ids)

        logger.debug(f"{len(items_ids)} images was found.")
        logger.debug(f"{len(anns)} annotations was found.")

        if len(anns) == 0:
            logger.error(f"No annotations was found")
            return None, info
        bboxes = anns[0].get('bbox', None) is not None

        ds = {
            "item": [],
            "label": [],
            "location": [],
            "date_captured": []
        }
        if bboxes:
            ds["bbox"] = []
        for ann in anns:
            try:
                item = json_handler.load_items(ann['item_id'])[0]
                categ = json_handler.load_cats(ann['category_id'])[0]
                cat_path_splt = [int(x) for x in categ['category_path']
                                 .split(',') if x]
                labels = []
                for cat_id in list(reversed(cat_path_splt)):
                    if len(categories) == 0 or cat_id in cat_ids:
                        cat = json_handler.load_cats(cat_id)[0]
                        labels.append(cat['name'])
                        if not multilabel:
                            break
                for label in labels:
                    item_name = get_item_name(item['id'], images_folder)
                    ds["item"].append(item_name)
                    ds["label"].append(label)
                    ds["location"].append(item['location'])
                    ds["date_captured"].append(item['date_captured'])
                    if bboxes:
                        bbox_str = ','.join([str(x) for x in ann['bbox']])
                        ds["bbox"].append(bbox_str)
            except:
                logger.exception("Exception ocurred in datasets/images")
                continue
        columns = ["item", "label", "location", "date_captured"]
        if bboxes:
            columns.append("bbox")
        data = pd.DataFrame(ds, columns=columns)
        info["categories"] = categories
        info["dataset_type"] = "object_detection" if bboxes \
            else 'classification'

        return data, info

    def filesystem_writer(self, row, **kwargs):
        dest_folder = kwargs['dest_folder']
        keep_originals = kwargs.get('keep_originals', True)
        item_name = os.path.basename(row['item'])
        dest_item = os.path.join(dest_folder, item_name)
        try:
            copy2(row['item'], dest_item)
        except:
            return
        if not keep_originals:
            os.remove(row['item'])
            row['item'] = dest_item

    def to_tfrecords(self, path, num_shards=5):
        """Creates TFRecord files from the dataset

        Parameters
        ----------
        path : str
            Path where the tfrecords will be stored.
        num_shards : int, optional
            The number of shards per dataset split (default is 5)

        """
        logger.info(f"Convertig dataset to tfrecords")
        try:
            import tensorflow as tf
        except ImportError:
            logger.warning(
                "You must hace Tensorflow installed. Trying to install")
            subprocess.call(
                [sys.executable, "-m", "pip", "install", 'tensorflow'])
            import tensorflow as tf
        from .object_detection.utils import label_map_util

        if not os.path.exists(path):
            os.makedirs(path)
        partitions = [x for x in self.params["partitions"].keys()]
        if not partitions:
            return

        dataset_type = self.info.get("dataset_type", "classification")
        logger.debug(f"Dataset type: {dataset_type}")
        if dataset_type == 'object_detection':
            df = self.as_dataframe(
                columns=["item", "label", "bbox"], splitted=False)
            class_names = sorted([x for x in set(df["label"].values)])
            label_map_list = \
                [{"name": x[0], "id": x[1]+1}
                 for x in zip(class_names, range(len(class_names)))]
            label_map_file = os.path.join(path, 'labelmap.pbtxt')
            write_pbtxt(label_map_list, label_map_file)
            label_map_dict = label_map_util.get_label_map_dict(label_map_file)
            for partition in partitions:
                files_labels = \
                    [{"image": x[0], "label": x[1], "bbox": x[2]}
                     for x in df.loc[df['partition'] == partition].values]
                imgs_to_anns = {}
                for file_label in files_labels:
                    if file_label['image'] not in imgs_to_anns:
                        imgs_to_anns[file_label['image']] = []
                    imgs_to_anns[file_label['image']].append({
                        'bbox': file_label['bbox'],
                        'label': file_label['label'],
                    })
                create_tfrecord(
                    partition, imgs_to_anns, label_map_dict, path, num_shards)
        else:
            df = self.as_dataframe(columns=["item", "label"], splitted=False)
            class_names = sorted([x for x in set(df["label"].values)])
            class_names_to_ids = dict(
                zip(class_names, range(len(class_names))))
            for partition in partitions:
                files_labels = \
                    [{"image": x[0], "label": x[1]}
                     for x in df.loc[df['partition'] == partition].values]
                self.create_partition(
                    partition, files_labels, class_names_to_ids,
                    path, num_shards)
            self.write_label_file(class_names, path)
        logger.info('Finished converting the dataset!')


def create_tfrecord(split_name,
                    imgs_to_anns,
                    label_map_dict,
                    tfrecords_dir,
                    num_shards=5):
    from .object_detection.dataset_tools import tf_record_creation_util

    output_filename = os.path.join(tfrecords_dir, '%s.record' % split_name)
    with contextlib2.ExitStack() as tf_record_close_stack:
        output_tfrecords = \
            tf_record_creation_util.open_sharded_output_tfrecords(
                tf_record_close_stack, output_filename, num_shards)
        idx = 0
        for image_path, anns in imgs_to_anns.items():
            if idx % 100 == 0:
                logger.info('On image %d of %d', idx, len(imgs_to_anns))
            try:
                tf_example = dict_to_tf_example(
                    image_path, anns, label_map_dict)
                if tf_example:
                    shard_idx = idx % num_shards
                    output_tfrecords[shard_idx].write(
                        tf_example.SerializeToString())
            except ValueError:
                logger.warning('Invalid example: %s, ignoring.', xml_path)
            idx += 1


def dict_to_tf_example(image_path,
                       annotations,
                       label_map_dict):
    import tensorflow as tf
    from .object_detection.utils import dataset_util

    filename = os.path.abspath(image_path)
    with tf.gfile.GFile(filename, 'rb') as fid:
        encoded_jpg = fid.read()
    image = Image.open(io.BytesIO(encoded_jpg))
    width, height = image.size
    image_format = b'jpg'
    xmins = []
    ymins = []
    xmaxs = []
    ymaxs = []
    classes = []
    clsses_txt = []
    for annotation in annotations:
        bbox_split = annotation['bbox'].split(',')
        # TODO: change float to int when scaled to width and height
        xmin = float(bbox_split[0])
        ymin = float(bbox_split[1])
        ann_width = float(bbox_split[2])
        ann_height = float(bbox_split[3])
        xmax = xmin + ann_width
        ymax = ymin + ann_height
        xmins.append(xmin)  # / width)
        ymins.append(ymin)  # / height)
        xmaxs.append(xmax)  # / width)
        ymaxs.append(ymax)  # / height)
        classes.append(label_map_dict[annotation['label']])
        clsses_txt.append(annotation['label'].encode('utf8'))

    feature_dict = {
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename.encode('utf8')),
        'image/source_id': dataset_util.bytes_feature(filename.encode('utf8')),
        'image/encoded': dataset_util.bytes_feature(encoded_jpg),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(clsses_txt),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }
    example = tf.train.Example(
        features=tf.train.Features(feature=feature_dict))
    return example


def write_pbtxt(label_map_list, output_name):
    if os.path.isfile(output_name):
        os.remove(output_name)
    end = '\n'
    s = ' '
    for category in label_map_list:
        out = ''
        out += 'item' + s + '{' + end
        out += s*2 + 'id:' + ' ' + str(category["id"]) + end
        out += s*2 + 'name:' + ' ' + '\'' + category["name"] + '\'' + end
        out += '}' + end*2
        with open(output_name, 'a') as f:
            f.write(out)


def create_partition(split_name,
                     files_labels,
                     class_names_to_ids,
                     tfrecords_dir,
                     num_shards=5):
    n = len(files_labels)
    num_per_shard = int(math.ceil(n / float(num_shards)))

    with tf.Graph().as_default():
        image_reader = ImageReader()
        sess = tf.Session()

        for shard_id in range(num_shards):
            tfrecord_name = \
                '%s_%05d-of-%05d.tfrecord' % (split_name, shard_id, num_shards)
            tfrecord_path = os.path.join(tfrecords_dir, tfrecord_name)
            with tf.python_io.TFRecordWriter(tfrecord_path) as tfrec_writer:
                start_ndx = shard_id * num_per_shard
                end_ndx = min((shard_id+1) * num_per_shard, n)
                for i in range(start_ndx, end_ndx):
                    sys.stdout.write('\r>> Converting image %d/%d shard %d'
                                     % (i+1, n, shard_id))
                    sys.stdout.flush()
                    try:
                        image_name = files_labels[i]["image"]
                        with tf.gfile.GFile(image_name, 'rb') as fid:
                            image_data = fid.read()
                        height, width = image_reader.read_image_dims(
                            sess, image_data)
                        class_name = files_labels[i]["label"]
                        class_id = class_names_to_ids[class_name]

                        example = image_to_tfexample(
                            image_data, b'jpg', height, width, class_id)
                        tfrec_writer.write(example.SerializeToString())
                    except Exception:
                        continue
    sys.stdout.write('\n')
    sys.stdout.flush()


def image_to_tfexample(image_data,
                       image_format,
                       height,
                       width,
                       class_id):
    return tf.train.Example(features=tf.train.Features(feature={
        'image/encoded': bytes_feature(image_data),
        'image/format': bytes_feature(image_format),
        'image/class/label': int64_feature(class_id),
        'image/height': int64_feature(height),
        'image/width': int64_feature(width)
    }))


def bytes_feature(values):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[values]))


def int64_feature(values):
    if not isinstance(values, (tuple, list)):
        values = [values]
    return tf.train.Feature(int64_list=tf.train.Int64List(value=values))


def write_label_file(class_names,
                     dataset_dir,
                     filename='labels.txt'):
    labels_to_class_names = dict(zip(range(len(class_names)), class_names))
    labels_filename = os.path.join(dataset_dir, filename)
    with tf.gfile.Open(labels_filename, 'w') as f:
        for label in labels_to_class_names:
            class_name = labels_to_class_names[label]
            f.write('%d:%s\n' % (label, class_name))


class ImageReader(object):
    def __init__(self):
        # Initializes function that decodes RGB JPEG data.
        self._decode_jpeg_data = tf.placeholder(dtype=tf.string)
        self._decode_jpeg = tf.image.decode_jpeg(
            self._decode_jpeg_data, channels=3)

    def read_image_dims(self, sess, image_data):
        image = self.decode_jpeg(sess, image_data)
        return image.shape[0], image.shape[1]

    def decode_jpeg(self, sess, image_data):
        image = sess.run(
            self._decode_jpeg, feed_dict={self._decode_jpeg_data: image_data})
        assert len(image.shape) == 3
        assert image.shape[2] == 3
        return image


class JsonHandler():
    """ JSON File Handler.
    """

    def __init__(self, json_data):
        self.data = dict()
        self.anns = dict()
        self.cats = dict()
        self.items = dict()
        self.items_to_anns = defaultdict(list)
        self.cats_to_items = defaultdict(list)
        self.data = json_data
        self.load_data()

    def load_data(self):
        logger.info('Creating json handler...')
        anns = {}
        cats = {}
        items = {}
        info = {}
        items_to_anns = defaultdict(list)
        cats_to_items = defaultdict(list)
        if 'info' in self.data:
            info = self.data['info']
        if 'annotations' in self.data:
            for ann in self.data['annotations']:
                items_to_anns[ann['item_id']].append(ann)
                anns[ann['id']] = ann
        if 'items' in self.data:
            for item in self.data['items']:
                items[item['id']] = item
        if 'categories' in self.data:
            for cat in self.data['categories']:
                cats[cat['id']] = cat
        if 'annotations' in self.data and 'categories' in self.data:
            for ann in self.data['annotations']:
                if ann['item_id'] not in cats_to_items[ann['category_id']]:
                    cats_to_items[ann['category_id']].append(ann['item_id'])
        logger.info('Handler created!')
        self.anns = anns
        self.items = items
        self.cats = cats
        self.items_to_anns = items_to_anns
        self.cats_to_items = cats_to_items
        self.info = info

    def print_info(self):
        """Print information about the annotation file.
        """
        for key, value in self.info.items():
            logger.info('{}: {}'.format(key, value))

    def get_ann_ids(self, item_ids=[], cat_ids=[], by_supercats=False):
        """Get ann ids that satisfy given filter conditions. default skips that
        filter.

        Parameters
        ----------
        item_ids : list of str, optional
          List of item ids to filter (default is [])
        cat_ids : list of int, optional
          List of category ids to filter (default is [])
        by_supercats : bool, optional
          Whether to compare also with supercategories (default is False)

        Returns
        -------
        list of str
          List of annotation ids
        """
        item_ids = item_ids if is_array_like(item_ids) else [item_ids]
        cat_ids = cat_ids if is_array_like(cat_ids) else [cat_ids]

        if len(item_ids) == len(cat_ids) == 0:
            anns = self.data['annotations']
        else:
            if not len(item_ids) == 0:
                lists = [self.items_to_anns[item_id] for item_id in item_ids
                         if item_id in self.items_to_anns]
                anns = list(itertools.chain.from_iterable(lists))
            else:
                anns = self.data['annotations']
            if not len(cat_ids) == 0:
                anns_nw = []
                for ann in anns:
                    cat_id = ann['category_id']
                    if by_supercats:
                        category_path = self.cats[cat_id]['category_path']
                        if any(x and int(x) in cat_ids
                               for x in category_path.split(',')):
                            anns_nw.append(ann)
                    else:
                        if cat_id in cat_ids:
                            anns_nw.append(ann)
                anns = anns_nw
        ids = [ann['id'] for ann in anns]
        return ids

    def get_cat_ids(self, cat_nms=[], cat_ids=[]):
        """Filtering parameters. default skips that filter.

        Parameters
        ----------
        cat_nms : list of str, optional
          List of category names to filter (default is [])
        cat_ids : list of int, optional
          List of category ids to filter (default is [])

        Returns
        -------
        list of int
          List of category ids
        """
        cat_nms = cat_nms if is_array_like(cat_nms) else [cat_nms]
        cat_ids = cat_ids if is_array_like(cat_ids) else [cat_ids]

        if len(cat_nms) == len(cat_ids) == 0:
            cats = self.data['categories']
        else:
            cats = self.data['categories']
            cat_nms = [cat_nm.lower() for cat_nm in cat_nms]
            cats = cats if len(cat_nms) == 0 else \
                [cat for cat in cats if cat['name'].lower() in cat_nms]
            cats = cats if len(cat_ids) == 0 else \
                [cat for cat in cats if cat['id'] in cat_ids]
        ids = [cat['id'] for cat in cats]
        return ids

    def get_item_ids(self, item_ids=[], cat_ids=[], by_supercats=False):
        """Get item ids that satisfy given filter conditions.

        Parameters
        ----------
        item_ids : list of str, optional
          List of item ids to filter (default is [])
        cat_ids : list of int, optional
          List of category ids to filter (default is [])
        by_supercats : bool, optional
          Whether to compare also with supercategories (default is False)

        Returns
        -------
        list of str
          List of item ids
        """
        item_ids = item_ids if is_array_like(item_ids) else [item_ids]
        cat_ids = cat_ids if is_array_like(cat_ids) else [cat_ids]

        if len(item_ids) == len(cat_ids) == 0:
            ids = self.items.keys()
        else:
            ids = []
            anns = self.data['annotations']
            for ann in anns:
                if ann['item_id'] in ids:
                    continue
                cat_id = ann['category_id']
                if by_supercats:
                    category_path = self.cats[cat_id]['category_path']
                    if any(x and int(x) in cat_ids
                            for x in category_path.split(',')):
                        ids.append(ann['item_id'])
                else:
                    if cat_id in cat_ids:
                        ids.append(ann['item_id'])
        return ids

    def load_anns(self, ids=[]):
        """Load anns with the specified ids.

        Parameters
        ----------
        ids : list of str, optional
          List of annotations ids to filter (default is [])

        Returns
        -------
        list of dict
          List of annotation dicts
        """
        if is_array_like(ids):
            return [self.anns[id] for id in ids]
        else:
            return [self.anns[ids]]

    def load_cats(self, ids=[]):
        """Load cats with the specified ids.

        Parameters
        ----------
        ids : list of str, optional
          List of category ids to filter (default is [])

        Returns
        -------
        list of dict
          List of category dicts
        """
        if is_array_like(ids):
            return [self.cats[id] for id in ids]
        else:
            return [self.cats[ids]]

    def load_items(self, ids=[]):
        """Load anns with the specified ids.

        Parameters
        ----------
        ids : list of str, optional
          List of item ids to filter (default is [])

        Returns
        -------
        list of dict
          List of item dicts
        """
        if is_array_like(ids):
            return [self.items[id] for id in ids]
        else:
            return [self.items[ids]]

    def download(self, results_dir=None, item_ids=[]):
        """Download items from server.

        Parameters
        ----------
        results_dir : str, optional
          Path to results dir (default is None)
        item_ids : list of str, optional
          List of item ids to filter (default is [])

        """
        if results_dir is None:
            logger.error('Please specify target directory')
            return -1
        if len(item_ids) == 0:
            items = self.items.values()
        else:
            items = self.load_items(item_ids)
        if not os.path.exists(results_dir):
            os.makedirs(results_dir)
        urls_filenames = []
        for i, item in enumerate(items):
            fname = get_item_name(item['id'], results_dir)
            urls_filenames.append((item['url'], fname))
        parallel_download(urls_filenames)


def get_item_name(item_id, path):
    filename = '%s.JPG' % item_id
    return os.path.join(path, filename)
