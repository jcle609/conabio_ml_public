#CONABIO ML API

CONABIO ML API is a free software project to facilitate the work in tasks of Machine Learning, using the information generated in CONABIO and other projects of biodiversity conservation.

This programming interface provides the Dataset class, which facilitates the management of data sets, as well as the classes for performing operations on them and which can be used in an machine learning experiment.

The API also allows you to record the operations performed in each stage of the experiment so that it is possible to replicate at any time.

##Description

CONABIO ML API allows you to create instances of Datasets regardless of the source of the data (databases, REST APIs, files in JSON or CSV format, file systems) and convert them to the input formats of the Machine Learning libraries commonly used by the community. With this, it is possible to use the data generated by CONABIO with any of these libraries, and in the same way, allows to use data from other sources in an experiment with the models provided by CONABIO ML API.

The Dataset class is consistent for data of different formats (image, text, audio, video, etc.), so the range of experiments that can be done with this tool is quite broad.

##Getting Started

###Installation

####Environment setup

The easiest (and the recommended) way to install the CONABIO_ML API is through a virtual environment that has to run over Python 3.6+

You can use *venv* in a folder by executing the command:

    python3 -m venv path-to-virtual-environment

After activating the virtual environment, with the command:

    source path-to-virtual-environment/bin/activate

Install the repository by using:

    pip install -e git+https://rriverac@bitbucket.org/conabio_cmd/conabio_ml.git@CONML-18-crear-el-mdulo-datasets-del-api#egg=conabio_ml

Note: The option "-e" is used to also download the source code, which typically you found in src/conabio-ml path.

####Testing

To check if the API was correctly installed, you can run the set of unit tests by executing:

    nosetests -c nose.cfg


##Documentation

You will find complete  documentation with the API Reference and useful examples at [the Read the Docs site](https://conabio-ml-public.readthedocs.io/es/latest/index.html).