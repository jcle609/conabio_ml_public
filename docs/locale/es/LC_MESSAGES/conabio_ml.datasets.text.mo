��                        �     �  3     8   :  <   s  )   �  *   �  0     3   6     j  B   |  *   �  ;   �     &     >  6   [     �  
   �  P   �     �     
  �     m   �  
     3        A  !   _  )   �  (   �  �  �     U  3   n  8   �  <   �  )   	  *   B	  0   m	  3   �	     �	  B   �	  *   '
  ;   R
     �
     �
  6   �
     �
  
   
  P        f     r  �   z  m   �  
   j  3   u     �  !   �  )   �  (      (pandas.DataFrame, dict) Bases: :class:`conabio_ml.datasets.dataset.Dataset` Bases: :class:`conabio_ml.datasets.text.dataset.Dataset` Builds a filtered dataset from the data loads from json file Categories in test partition (default []) Categories in train partition (default []) Categories in val partition (default empty list) Creates an instance of MWS Dataset from a json file Data in json file Dataset of type Text contains functionality only used in NLP tasks Destination folder to stores auxiliar data If categories sum more than dataset items a WARNING appears Instance of the Dataset List of categories to filter List of categories to filter registers (default is []) Module contents Parameters Path to a json file with the correct format to be converted to a Dataset object. Return type Returns Since MWS is an specific scenario of Text Dataset it inherits from it, and loads all information defined in its JSON Dataset file Split the dataset between `train`, `test` and `validation` partitions using sent categories in each partition Submodules The location path where the dataset will be stored. and retrieves it as dataframa conabio\_ml.datasets.text package conabio\_ml.datasets.text.builders module conabio\_ml.datasets.text.dataset module Project-Id-Version: API CONABIO ML 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-05-07 22:20-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 (pandas.DataFrame, dict) Bases: :class:`conabio_ml.datasets.dataset.Dataset` Bases: :class:`conabio_ml.datasets.text.dataset.Dataset` Builds a filtered dataset from the data loads from json file Categories in test partition (default []) Categories in train partition (default []) Categories in val partition (default empty list) Creates an instance of MWS Dataset from a json file Data in json file Dataset of type Text contains functionality only used in NLP tasks Destination folder to stores auxiliar data If categories sum more than dataset items a WARNING appears Instance of the Dataset List of categories to filter List of categories to filter registers (default is []) Module contents Parameters Path to a json file with the correct format to be converted to a Dataset object. Return type Returns Since MWS is an specific scenario of Text Dataset it inherits from it, and loads all information defined in its JSON Dataset file Split the dataset between `train`, `test` and `validation` partitions using sent categories in each partition Submodules The location path where the dataset will be stored. and retrieves it as dataframa conabio\_ml.datasets.text package conabio\_ml.datasets.text.builders module conabio\_ml.datasets.text.dataset module 