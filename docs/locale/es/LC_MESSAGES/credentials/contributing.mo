��          |               �   �   �   �   �     P  �   ]  
   �  M   �  E   7     }  <   �    �  �   �  �  �  �   K  �        �  �   �  
   L	  M   W	  E   �	     �	  <    
    =
  �   Y   *For typos, please do not create a pull request. Instead, declare them in issues or email the repository owner*. Please note we have a code of conduct, please follow it in all your interactions with the project. Add comments with details of changes to the interface, this includes new environment variables, exposed ports, useful file locations and container parameters. Contributing Ensure any install or build dependencies are removed before the end of the layer when doing a build and creating a pull request. Final Note Please consider the following criterions in order to help us in a better way: Please make sure your suggested resources are not obsolete or broken. Pull Request Process The pull request is mainly expected to be a link suggestion. We are looking forward to your kind feedback. Please help us to improve this open source project and make our work better. For contribution, please create a pull request and we will investigate it promptly. Once again, we appreciate your kind feedback and elaborate code inspections. You may merge the Pull Request in once you have the sign-off of at least one other developer, or if you do not have permission to do that, you may request the owner to merge it for you if you believe all checks are passed. Project-Id-Version: API CONABIO ML 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-05-07 22:20-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 *For typos, please do not create a pull request. Instead, declare them in issues or email the repository owner*. Please note we have a code of conduct, please follow it in all your interactions with the project. Add comments with details of changes to the interface, this includes new environment variables, exposed ports, useful file locations and container parameters. Contributing Ensure any install or build dependencies are removed before the end of the layer when doing a build and creating a pull request. Final Note Please consider the following criterions in order to help us in a better way: Please make sure your suggested resources are not obsolete or broken. Pull Request Process The pull request is mainly expected to be a link suggestion. We are looking forward to your kind feedback. Please help us to improve this open source project and make our work better. For contribution, please create a pull request and we will investigate it promptly. Once again, we appreciate your kind feedback and elaborate code inspections. You may merge the Pull Request in once you have the sign-off of at least one other developer, or if you do not have permission to do that, you may request the owner to merge it for you if you believe all checks are passed. 