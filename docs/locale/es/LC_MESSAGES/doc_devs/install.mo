��    
      l               �   y   �   <   7     t  !   �     �     �  �   �  ]   C  ;   �  �  �  y   ^  <   �       !   '     I     V  �   ^  ]   �  ;   B   **Note:** The option **"-e"** is used to also download the source code, which typically you found in src/conabio-ml path. After activating the virtual environment, with the command:: Environment setup Install the repository by using:: Installation Testing The easiest (and the recommended) way to install the CONABIO_ML API is through a virtual environment that has to run over Python 3.6+ To check if the API was correctly installed, you can run the set of unit tests by executing:: You can use **venv** in a folder by executing the command:: Project-Id-Version: API CONABIO ML 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-05-07 22:20-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 **Note:** The option **"-e"** is used to also download the source code, which typically you found in src/conabio-ml path. After activating the virtual environment, with the command:: Environment setup Install the repository by using:: Installation Testing The easiest (and the recommended) way to install the CONABIO_ML API is through a virtual environment that has to run over Python 3.6+ To check if the API was correctly installed, you can run the set of unit tests by executing:: You can use **venv** in a folder by executing the command:: 