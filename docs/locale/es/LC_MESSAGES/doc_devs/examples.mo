��          �               �  C   �  9     0   K     |  "   �     �     �  ?   �  	   -  	   7  	   A     K     [     b     g  A   }  4   �  >   �  Z   3  �   �  &   j  \   �  �   �     �  m   �     >  �  W  C   �  9   	  0   V	     �	  "   �	     �	     �	  ?   �	  	   8
  	   B
  	   L
     V
     f
     m
     r
  A   �
  4   �
  >   �
  Z   >  �   �  &   u  \   �  �   �     �  m   �     I   A partition is made on the dataset with the following proportions:: A representation in CSV files of the dataset is created:: A representation of dataset folders is created:: API modules corresponding to Auxiliary modules in the process:: Collections on server Dataset MWS del tipo Texto Datasets are created from the representations generated above:: Example 1 Example 2 Example 3 General Dataset Images Text Text type MWS Dataset The available collections of text type of the server are listed:: The collection of photos of the SNMB is downloaded:: The collections of images found in the repository are listed:: The dataset is converted to a representation of a DataFrame and some records are printed:: The existence of a dataset in folder source is assumed Files without extension are obtained from folder and separated by folder It is returned as a dataframe by default (All columns and with the name of the partition):: The necessary libraries are imported:: The same is done but this time it is indicated that you do not want to split the DataFrane:: They are returned as separate dataframe in train and test that contains categories 1 and 2 Train contains only category 1 and test with categories 2 The return dataframe only contains the item and label columns:: Use examples With the file that has just been downloaded, an instance of a Dataset is created based on specific criteria:: You get the collection:: Project-Id-Version: API CONABIO ML 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-05-07 22:20-0500
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 A partition is made on the dataset with the following proportions:: A representation in CSV files of the dataset is created:: A representation of dataset folders is created:: API modules corresponding to Auxiliary modules in the process:: Collections on server Dataset MWS del tipo Texto Datasets are created from the representations generated above:: Example 1 Example 2 Example 3 General Dataset Images Text Text type MWS Dataset The available collections of text type of the server are listed:: The collection of photos of the SNMB is downloaded:: The collections of images found in the repository are listed:: The dataset is converted to a representation of a DataFrame and some records are printed:: The existence of a dataset in folder source is assumed Files without extension are obtained from folder and separated by folder It is returned as a dataframe by default (All columns and with the name of the partition):: The necessary libraries are imported:: The same is done but this time it is indicated that you do not want to split the DataFrane:: They are returned as separate dataframe in train and test that contains categories 1 and 2 Train contains only category 1 and test with categories 2 The return dataframe only contains the item and label columns:: Use examples With the file that has just been downloaded, an instance of a Dataset is created based on specific criteria:: You get the collection:: 