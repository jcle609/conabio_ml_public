conabio\_ml.datasets package
============================

Subpackages
-----------

.. toctree::

    conabio_ml.datasets.images
    conabio_ml.datasets.text

Submodules
----------

conabio\_ml.datasets.collections module
---------------------------------------

.. automodule:: conabio_ml.datasets.collections
    :members:
    :undoc-members:
    :show-inheritance:

conabio\_ml.datasets.dataset module
-----------------------------------

.. automodule:: conabio_ml.datasets.dataset
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: conabio_ml.datasets
    :members:
    :undoc-members:
    :show-inheritance:
