conabio\_ml.datasets.images.object\_detection.protos package
============================================================

Submodules
----------

conabio\_ml.datasets.images.object\_detection.protos.string\_int\_label\_map\_pb2 module
----------------------------------------------------------------------------------------

.. automodule:: conabio_ml.datasets.images.object_detection.protos.string_int_label_map_pb2
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: conabio_ml.datasets.images.object_detection.protos
    :members:
    :undoc-members:
    :show-inheritance:
