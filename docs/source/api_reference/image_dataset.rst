Images Datasets
===========================


The methods and properties of the `` ImageDataset`` and `` JsonHandler`` classes are described below:

.. autoclass:: conabio_ml.datasets.images.datasets.ImageDataset
   :members:
   :inherited-members: