Collections
===========================

The methods and properties of the ``Collection`` class are listed below:

.. automodule:: conabio_ml.datasets.collections
   :members:
   :inherited-members: