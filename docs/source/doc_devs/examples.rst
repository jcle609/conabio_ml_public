Use examples 
===============


Images
------

The necessary libraries are imported::

    import os
    from conabio_ml.datasources.images.builders import build
    from conabio_ml.datasets.collections import Collection
    from conabio_ml.datasets.images import ImageDataset

The collections of images found in the repository are listed::

    collections = Collection.list(Collection.TYPES.Images)

The collection of photos of the SNMB is downloaded::

    dataset_path = "test"
    if not os.path.isdir(dataset_path):
        os.makedirs(dataset_path)
    dataset_file = Collection.Images.fetch(dataset_path)

With the file that has just been downloaded, an instance of a Dataset is created based on specific criteria::

    snmb_dataset = ImageDataset.from_json(
        destination_folder=dataset_path,
        json_file=dataset_file,
        categories=["Accipitridae", "Scolopacidae", "Tyrannidae"])

A partition is made on the dataset with the following proportions::

    snmb_dataset.split(train_perc=0.7, test_perc=0.2, val_perc=0.1, max_secs_in_sequences=2)

The dataset is converted to a representation of a DataFrame and some records are printed::

    dfs = snmb_dataset.as_dataframe(
        columns=["item", "label", "bbox"], 
        splitted=True)
    for df in dfs:
        print(df.head(5))

The same is done but this time it is indicated that you do not want to split the DataFrame::

    df = snmb_dataset.as_dataframe(
        columns=["item", "label", "bbox"],
        splitted=False)
    print(df.head(10))

A representation of dataset folders is created::

    path_folder = f"{dataset_path}/to_folder"
    snmb_dataset.to_folder(
        path=path_folder,
        split_in_partitions=True,
        split_in_labels=True,
        keep_originals=True)

A representation in CSV files of the dataset is created::

    snmb_dataset.to_csv(
        dest=f"{dataset_path}/to_csv/splitted", 
        columns=["item", "label", "bbox"],
        header=False, 
        splitted=True)

    csv_path = f"{dataset_path}/to_csv/no_splitted"
    snmb_dataset.to_csv(
        dest=csv_path, 
        columns=["item", "label"],
        header=True, 
        splitted=False)

Datasets are created from the representations generated above::

    snmb_dataset_2 = ImageDataset.from_folder(
        source_folder=path_folder,
        extensions=['.jpg', '.JPG'],
        split_by_folder=True, 
        include_id=False)
    print(snmb_dataset_2.as_dataframe().head(10))
    print(snmb_dataset_2.as_dataframe().tail(10))

    snmb_dataset3 = ImageDataset.from_csv(
        csv_source=csv_path,
        columns=[0, 1],
        header=False,
        # split_by_column=3,
        column_mapping={0: 'item', 1: 'label'})
    print(snmb_dataset3.as_dataframe().head(10)


Text
----

Auxiliary modules in the process::

    import copy
    import pandas
    import pydash

API modules corresponding to

    - General Dataset
    - Dataset MWS del tipo Texto
    - Text type MWS Dataset
    - Collections on server

::

    from conabio_ml.datasets.text import Dataset
    from conabio_ml.datasets.text import MWS
    from conabio_ml.datasets.collections import Collection

The available collections of text type of the server are listed::

    collections = Collection.list(Collection.TYPES.Text)
    print (collections)

You get the collection::

    dataset_file = Collection.Text.fetch(
        destination_folder = "/Users/rrivera/Desktop/API_examples/collection", 
        year=2019, 
        version=1.0, 
        collection='mws')

Example 1
+++++++++

They are returned as separate dataframe in train and test that contains categories 1 and 2
Train contains only category 1 and test with categories 2
The return dataframe only contains the item and label columns::

    train, test = MWS.from_json(
        json_file=dataset_file,
        destination_folder="",
        categories=[1, 2])\
            .cross_category_split(train_cats=[2], test_cats=[1])\
            .as_dataframe(columns=["item", "label"], splitted=True)
    train.head(5)

Example 2
+++++++++

The existence of a dataset in folder source is assumed
Files without extension are obtained from folder and separated by folder
It is returned as a dataframe by default (All columns and with the name of the partition)::

    source_folder = "/Users/rrivera/Desktop/API_examples/news_fake"
    dataset = Dataset.from_folder(
        source_folder=source_folder,
        extensions=[""],
        split_by_folder=True)\
            .as_dataframe()
    dataset.head(5)

    source_folder = "/Users/rrivera/Desktop/API_examples/news"
    dataset = Dataset.from_folder(source_folder=source_folder,
                                    extensions=[""],
                                    split_by_folder=False)\
            .as_dataframe()
    dataset

Example 3
+++++++++

The existence of a dataset in folder source is assumed
Files without extension are obtained from folder and separated by folder
It is returned as a dataframe by default (All columns and with the name of the partition)::

    source = "/Users/rrivera/Desktop/API_examples/csv"
    dataset = Dataset.from_csv(
        csv_source=source,
        columns=["id", "data", "cat_ids"],
        column_mapping={"data": "item",
                        "cat_ids": "label"})\
            .split()

    dataset.as_dataframe(columns=["item", "label"])

