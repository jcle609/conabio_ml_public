Instalación
*************

Preparación del ambiente
========================

La manera más fácil (y la que recomendamos) para instalar el API CONABIO_ML es a través de un ambiente virtual, el cual debe correr sobre Python 3.6+.

El ambiente virtual puede crearse utilizando **venv** por medio de la ejecución del comando::

  python3 -m venv /path/to/virtual/environment

Después de activar el ambiente virtual, con el comando::

  source /path-to-virtual-environment/bin/activate

Puede instalar el repositoro usando::

  pip install git+https://rriverac@bitbucket.org/conabio_cmd/conabio_ml.git@dev

Si es necesaria la obtención del código fuente además de la instalación, puede realizarlo con::

  git clone -b dev https://rriverac@bitbucket.org/conabio_cmd/conabio_ml.git
  cd conabio_ml
  python setup.py install


Pruebas
========

Para revisar si el API fue correctamente instalado, puede correr las pruebas unitarias ejecutando::

  nosetests -c nose.cfg