Installation
*************

Environment setup
==================

The easiest (and the recommended) way to install the CONABIO_ML API is through a virtual environment that has to run over Python 3.6+

You can use **venv** in a folder by executing the command::

  python3 -m venv /path/to/virtual/environment

After activating the virtual environment, with the command::

  source /path-to-virtual-environment/bin/activate

You can install the repository by using::

  pip install git+https://rriverac@bitbucket.org/conabio_cmd/conabio_ml.git@dev

If also, aside the installation you need the source code, you can use::

  git clone -b dev https://rriverac@bitbucket.org/conabio_cmd/conabio_ml.git
  cd conabio_ml
  python setup.py install


Testing
========

To check if the API was correctly installed, you can run the set of unit tests by executing::

  nosetests -c nose.cfg