Unis tests
==========

To carry out the unit tests, it is necessary to download the project with the following instruction in a System Terminal::

    git clone -b CONML-18-crear-el-mdulo-datasets-del-api --single-branch https://jcle609@bitbucket.org/conabio_cmd/conabio_ml.git

Then we change to the project directory::

    cd conabio_ml

We execute the unit tests with the module unittest *::

    python -m unittest test.py

