Data sets
=========

Dataset
-------

Datasets can be found in a wide variety of formats, and not always in one that allows them to be part of the flow of an automatic learning experiment.

The Dataset class of the conabio-ml API provides a way to transform some of the most commonly used data set formats into a standard representation, allowing the same operations to be applied to the data regardless of the original format. This is especially useful because it allows decoupling the input data of the later stages of the experiment, thus being able, for example, to use a data format regardless of the library used in the training stage.

To do this, the conabio-ml API declares the Dataset class, which contains methods to load a dataset from a variety of different sources. The Dataset class can be inherited by other classes that implement the logic to convert from different data types (image, text, video, etc.) to the standard representation, which will be carried out in the build method of each implementation.

The following UML diagram shows the class hierarchy for implementations of the ImageDataset and TextDataset types.

.. figure:: ../imgs/Dataset_UML.png
   :align:   center

   UML diagrama of Dataset class and subclasses

The data property of Dataset is the one that will contain the standard representation as a DataFrame object of pandas. The Dataset build method is abstract and each of the specific classes must implement it, since it will be called from the from methods of Dataset and will return the data and info properties.

The info property must also be assigned in the build method and will contain information specific to the data set used. This information is free and can be used later to carry the track of operations applied to the Dataset in the pipeline of the experiment.

Each specific Dataset implementation will contain useful properties and methods to convert the information into the standard representation, and will depend on the nature of each data set.

The Dataset class contains methods that allow the standard representation to be converted into some of the data formats used in the most popular Learning libraries, and thus allow decoupling of the generation stage of data from the later stages of the experiment flow.

Some of the supported data formats to which it is possible to convert the standard representation are: TFRecords, CSV, file system folders, DataFrame, Numpy Arrays lists, etc.


Standard Representation
+++++++++++++++++++++++

The data property of Dataset will contain the standard representation of the data, through a DataFrame object of pandas, and will have the minimum information to determine the labels of each item and to which partition it belongs.

In this way you can move from a representation of the data in a specific format, to a standard tabular representation independent of the original format and that can easily feed a pipeline of Machine Learning.

.. figure:: ../imgs/any_format_to_std.png
   :align:   center

   Any format can be converted to the standard representation

For example, you can go from a representation of a dataset through a json file with the format proposed by the conabio-ml API, and through the from_json method of the Dataset class, create an instance of Dataset, which will contain the information in the standard format, which will be manipulated in the same way regardless of the original format.

.. figure:: ../imgs/ejemplo_json.png
   :align:   center

   Example of converting a file in JSON format to standard format


Text Dataset
------------

The Text DataSet is loaded by means of the conabio-ml.text.datasets.DataSet module from the DataSource provided by conabio-ml.text.datasources.DataSource.

Whose implementations are focused on the language process.

DataSources
+++++++++++

Text DataSources as well as other types inherit from the DataSource type.

And they follow the general structure::

    info {
        "year"      :   int,
        "version"       :   str,
        "description"   :   str,
        "contributor"   :   str,
        "url"       :   str,
        "date_created"  :   datetime
    }
    
    item {
        "id"        :   str,
        "file_id"   :   id,
        "data"      :   string,
        "ordinal"   :   int,
        "source"    :   string,
        "title"     :   string
        }
    
    annotation {
        "id"        :   id,
        "item_id"   :   id,
        "category_id"   :   id
        }  
    
    category {
        "id"        :   int,
        "name"      :   string
        }


To create a Text DataSource you need a class that inherits from the TextDataSource type::

    class datasource(DataSource):
        def __init__(self, path, info):
            try:
                super(datasource, self).__init__(path, info)
            except Exception as ex:
                raise
    
        def create_datasource(self, data):
        raise Exception("Method not implemented")

DataSets
++++++++

To create a text-based dataset, TextDataSet uses an instance of TextDataSource::

    class dataset(Dataset):
        def __init__(self, path, datasource, opts):
            try:
                super(dataset, self).__init__(path, datasource, opts)
            except Exception as ex:
                raise
    
        def build(self, datasource_instance, filter_opts):
        raise Exception("Method not implemented")

And load in the dataset property the csv corresponding to the data set, with the format:

.. figure:: ../imgs/tabla1_text_dataset.png
   :align:   center

   formato del csv

Types
+++++

Currently there is a kind of set:

    * DataSource for the species mining platform.

MWS
+++

The species mining DataSource contains paragraph-level tags for topic-type classes obtained from different information sources.

To create a DataSource with MWS annotations, it is done as follows::

    from datasets.text.datasets import TextDataset
    
    ds = TextDataset(
        info={"collection": "mws",
        "year": 2019,
        "version": 1.0})

This generates a file in the specified path:

    * mws_YEAR_VERSION.json


An example of the generated DataSource would be the following::

    {
        "info": {
            "description": "Mining Dataset web services",
            "version": "1.0",
            "date_created": "2019-02-06",
            "year": "2019",
            "url": "ftp://172.16.3.111/share/corpora/text/mws_2019_1.0.tar.gz",
            "contributor": "National System of Monitoring of Biodiversity",
            "type": "json"
        },
        "categories": [
            {
                "id": 1,
                "name": "species"
            }
            [ . . . ]
        ],
        "items": [
            {
                "id": "2606d1df-ee3e-4ef7-9ee0-119fc31382a0",
                "file_id": "2bcfd5d1-0c84-440a-a2ce-97fb26c016e2",
                "data": "Methylated [ . . . ] examination of DNA methylation states will be required to confirm that two major groups of pea aphid genes are differentially regulated by methylation.",
                "ordinal": 28,
                "source": "PLOS",
                "title": "Genome Sequence of the Pea Aphid Acyrthosiphon pisum"
            }
            [ . . . ]
        ],
        "annotations": [
            {
                "id": "2606d1df-ee3e-4ef7-9ee0-119fc31382a0",
                "item_id": "2606d1df-ee3e-4ef7-9ee0-119fc31382a0",
                "category_id": 1
            }
            [ . . . ]
        ]
    }

Where the field category_id contains a value of the categories available in category.

Finally, to build the DataSet from TextDataSource with the necessary options, we use the TextDataSet class, as follows::

    from datasets.text.datasets import TextDataset
    from datasets.text.datasets import Dataset
    
    path = "path"
    
    datasource = TextDataSource(path,
                    info={  "year": "2019",
                        "version": "1.0",
                        "collection": "mws"})
    dataset = TextDataset(  path=path,
                datasource=datasource,
                opts={})
    dataset.load()

An example of the DataSet is as follows:

.. figure:: ../imgs/tabla2_text_dataset.png
   :align:   center


Images Dataset
--------------

Dataset
+++++++

The conabio-ml API provides the datasets.images.builders.DatasetBuilder class, which can be used to create instances of specific datasets. In the following example, a SNMB dataset is created::

    from datasets.images.builders import SNMB_builder
    
    snmb_dataset_builder = SNMB_builder(
        categories=["Person", "Bos taurus",
                    "Equus caballus", "Mammalia",
                    "Aves", "Reptilia"],
        data_source_file=data_source_file,
        type='object_detection',
        multilabel=False,
        include_place_and_date=True

The above will create an instance of an SNMB dataset of type object_detection (with bounding boxes) in the path specified by path, from the data source specified in data_source_file (for example snmb_2019_1.0_bboxes.json) and with labels of the categories: Person , Bos taurus, Equus caballus, Mammalia, Birds and Reptilia.
It should be noted that the images of the Person, Bos taurus and Equus caballus categories will not be contained in the Mammalia category images. To change this behavior it is necessary to send the multilabel = True parameter to the constructor. This is useful for training models in which an image or bounding box can belong to several categories.

The categories parameter is a list that contains the valid scientific names (contained in the data source categories field) of some taxon, or the terms Person or Background.

In the path folder the following files and folders will be created:

**annotations.csv**: file that contains a record for each annotation, with the following columns::

    item_id:      str
    url:          str
    label:        str
    place (*):    str
    date  (*):    str
    bbox (**):    [x,y,width,height]
    
    
    (*)  Field that is added if the include_place_and_date = True option is sent in the dataset constructor
    (**) Field for datasets of type object_detection

descriptor.json: json file that contains the following fields of the data source and parameters with which the dataset was created::

    year:             int
    version:          str
    bboxes:           boolean
    labels:           [str]
    place_and_date    boolean

data: folder where the images that belong to the dataset are downloaded.



If a dataset has already been previously created in a folder, it is only necessary to specify it as a dataset constructor parameter::

    snmb_dataset_builder = SNMB_builder(path=path)

This will create an instance of the dataset with the parameters with which it was previously configured.
If the annotations file (annotations.csv) does not exist inside the path folder, it will be constructed again from the data source, whether it is provided through the data_source_file parameter of the constructor, or based on its name, constructed with the data stored in the descriptor file (year, version and bboxes).
The data_source_file file will be searched inside the path folder, and if it is not found, the .tar.gz file that contains it will be downloaded from the datasets FTP server and it will be unzipped into a folder within the path. In this way the annotation file will be generated and the missing images will be downloaded into the data folder.

An example file descriptor would be::

    {
        "dataset": {
            "version": "1.0",
            "year": 2019,
            "bboxes": true,
            "labels": [
                "Homo sapiens",
                "Bos taurus",
                "Equus caballus",
                "Mammalia",
                "Aves",
                "Reptilia"
            ],
            "format": "csv",
            "place_and_date": false
        }
    }

The csv file with the annotations would be:

.. figure:: ../imgs/tabla1_images_dataset.png
   :align:   center
