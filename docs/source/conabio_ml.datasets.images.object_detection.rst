conabio\_ml.datasets.images.object\_detection package
=====================================================

Subpackages
-----------

.. toctree::

    conabio_ml.datasets.images.object_detection.dataset_tools
    conabio_ml.datasets.images.object_detection.protos
    conabio_ml.datasets.images.object_detection.utils

Module contents
---------------

.. automodule:: conabio_ml.datasets.images.object_detection
    :members:
    :undoc-members:
    :show-inheritance:
