#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import copy
import pandas
import pydash

from conabio_ml.datasets.text.dataset import Dataset
from conabio_ml.datasets.text.dataset import MWS
from conabio_ml.datasets.collections import Collection


def run_text_test():
    print("Trying to acquire avialable text collections")
    collections = Collection.list(Collection.TYPES.Text)

    collection = collections[0]
    dataset_path = "test"
    print("Target", collection)

    if not os.path.isdir(dataset_path):
        os.makedirs(dataset_path)

    dataset_file = Collection.Text\
        .fetch(destination_folder=dataset_path,
               year=collection["year"],
               version=collection["version"],
               collection=collection["id"])

    train, test = MWS.from_json(json_file=dataset_file,
                                categories=[1, 2])\
        .cross_category_split(train_cats=[2], test_cats=[1])\
        .as_dataframe(columns=["item", "label"], splitted=True)

    print(train.head())
    print(test.head())

    print("Seems that everything is fine")
