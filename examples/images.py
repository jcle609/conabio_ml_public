import os

from conabio_ml.datasources.images.builders import build
from conabio_ml.datasets.collections import Collection
from conabio_ml.datasets.images.datasets import ImageDataset

def run_images_test():
    collections = Collection.list(Collection.TYPES.Images)
    dataset_path = "test"
    if not os.path.isdir(dataset_path):
        os.makedirs(dataset_path)
    dataset_file = Collection.Images.fetch(dataset_path)

    # snmb_dataset = ImageDataset.from_json(
    #     destination_folder=dataset_path,
    #     json_file=dataset_file,
    #     categories=["Accipitridae", "Scolopacidae", "Tyrannidae"],
    #     group_by_place_and_date=True)

    # snmb_dataset.split(train_perc=0.7, test_perc=0.2, val_perc=0.1)

    # dfs = snmb_dataset.as_dataframe(
    #     columns=["item", "label", "bbox"],
    #     splitted=True)
    # for df in dfs:
    #     print(df.head(5))

    # df = snmb_dataset.as_dataframe(
    #     columns=["item", "label", "bbox"],
    #     splitted=False)
    # print(df.head(10))

    # path_folder = f"{dataset_path}/to_folder"
    # snmb_dataset.to_folder(
    #     path=path_folder,
    #     split_in_partitions=True,
    #     split_in_labels=True,
    #     keep_originals=True)

    # snmb_dataset.to_csv(
    #     dest=f"{dataset_path}/to_csv/splitted",
    #     columns=["item", "label", "bbox"],
    #     header=False,
    #     splitted=True)

    # csv_path = f"{dataset_path}/to_csv/no_splitted"
    # snmb_dataset.to_csv(
    #     dest=csv_path,
    #     columns=["item", "label"],
    #     header=True,
    #     splitted=False)

    # snmb_dataset_2 = ImageDataset.from_folder(
    #     source_folder=path_folder,
    #     extensions=['.jpg', '.JPG'],
    #     split_by_folder=True,
    #     include_id=False)
    # print(snmb_dataset_2.as_dataframe().head(10))
    # print(snmb_dataset_2.as_dataframe().tail(10))

    # snmb_dataset3 = ImageDataset.from_csv(
    #     csv_source=csv_path,
    #     columns=[0, 1],
    #     header=False,
    #     # split_by_column=3,
    #     column_mapping={0: 'item', 1: 'label'})
    # print(snmb_dataset3.as_dataframe().head(10))



