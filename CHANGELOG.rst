Changelog
==========

All notable changes to this project will be documented in this file.

The format is based on `Keep a
Changelog <http://keepachangelog.com/en/1.0.0/>`__ and this project
adheres to `Semantic Versioning <http://semver.org/spec/v2.0.0.html>`__.

Version 0.0.1
=============

Released 2019-25-04

- Initial release, with modules:
    * Datasources
    * Datasets