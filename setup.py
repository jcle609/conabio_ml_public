import conabio_ml
import os
import setuptools
import shutil

from setuptools import setup, find_packages
from setuptools.command.develop import develop
from setuptools.command.install import install
from distutils.command.clean import clean as Clean

DISTNAME = 'conabio_ml'
DESCRIPTION = 'Pythom modules that help to develop ML experiments in CONABIO'
with open('README.md') as f:
    LONG_DESCRIPTION = f.read()
AUTHOR = 'Coordinación de Ecoinformática - CONABIO'
AUTHOR_EMAIL = "jlopez@conabio.gob.mx, ramon.rivera@conabio.gob.mx"
URL = 'https://bitbucket.org/conabio_cmd/conabio_ml'
DOWNLOAD_URL = 'https://bitbucket.org/conabio_cmd/conabio_ml'
PROJECT_URLS = {
    'WIKI': 'https://ecoinformatica.atlassian.net/wiki/spaces/CONML/overview'
}
LICENSE = 'GNU Affero General Public License'

VERSION = conabio_ml.__version__

NUMPY_MIN_VERSION = '1.14.0'
SCIKIT_MIN_VERSION = '0.20.3'
PYDASH_MIN_VERSION = '4.7.4'
PANDAS_MIN_VERSION = '0.24.2'
PILLOW_MIN_VERSION = '6.0.0'


class CleanCommand(Clean):
    description = "Remove build artifacts from the source tree"
    cwd = os.path.abspath(os.path.dirname(__file__))

    if os.path.exists('build'):
        shutil.rmtree('build')

    for dirpath, dirnames, filenames in os.walk('conabio_ml'):
        try:
            for filename in filenames:
                for suffix in (".so", ".pyd", ".dll", ".pyc"):
                    if filename.endswith(suffix):
                        os.unlink(os.path.join(dirpath, filename))
                        break

            for dirname in dirnames:
                if dirname == '__pycache__':
                    shutil.rmtree(os.path.join(dirpath, dirname))

        except Exception as ex:
            print(ex)


cmdclass = {'clean': CleanCommand}


def setup_package():
    metadata = dict(name=DISTNAME,
                    author=AUTHOR,
                    author_email=AUTHOR_EMAIL,
                    description=DESCRIPTION,
                    license=LICENSE,
                    url=URL,
                    download_url=DOWNLOAD_URL,
                    project_urls=PROJECT_URLS,
                    version=VERSION,
                    packages=find_packages(),
                    long_description=LONG_DESCRIPTION,
                    classifiers=['License :: GNU Affero General Public License',
                                 'Programming Language :: Python',
                                 'Topic :: Scientific/Engineering',
                                 'Operating System :: Microsoft :: Windows',
                                 'Operating System :: POSIX',
                                 'Operating System :: Unix',
                                 'Operating System :: MacOS',
                                 'Programming Language :: Python :: 3',
                                 'Programming Language :: Python :: 3.5',
                                 'Programming Language :: Python :: 3.6',
                                 ],
                    cmdclass=cmdclass,
                    install_requires=[
                        'numpy>={}'.format(NUMPY_MIN_VERSION),
                        'scikit-learn>={}'.format(SCIKIT_MIN_VERSION),
                        'pydash>={}'.format(PYDASH_MIN_VERSION),
                        'pandas>={}'.format(PANDAS_MIN_VERSION),
                        'Pillow>={}'.format(PILLOW_MIN_VERSION)
                    ])

    setuptools.setup(**metadata)


if __name__ == "__main__":
    setup_package()
